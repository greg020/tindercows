package tools

object CalvingDifficulty extends Enumeration {
  type CalvingDifficulty = Value

  val DIFFICULT, EASY = Value

  /**
    * Get the calving difficulty based on the value as parameter.
    * @param value The value we want to convert to calving difficulty.
    * @return DIFFICULT if over 100 and EASY otherwise.
    */
  def get(value : Int): CalvingDifficulty = if (value < 100) DIFFICULT else EASY
}
