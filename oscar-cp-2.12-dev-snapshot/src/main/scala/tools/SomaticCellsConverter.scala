package tools

object SomaticCellsConverter {

  val log2 = scala.math.log(2)

  def SCCtoSCS(scc : Double) : Double = {
    //log2(scc/100) + 3
    (scala.math.log(scc / 100) / log2) + 3
  }

  def SCStoSCC(scs : Double) : Double = {
    //2^(scs-3) * 100
    (scala.math.pow(2, scs-3) * 100)
  }
}
