package tools
import scala.collection.mutable

/**
  * Used when there are 2 possible dads for a bovine, in order to find the highest consanguinity.
  */
class Duality {

  var conflicts = new mutable.HashMap[String, Conflict]()
  var startCharacter = "a"

  /**
    * Return the value corresponding to a key.
    * @param name the key for which we want the value.
    * @return The value corresponding to the key.
    */
  private def eval(name: String): Array[Int] = {
    var conflict = conflicts.remove(name).get

    var aInt = toInt(conflict.a)
    var bInt = toInt(conflict.b)
    if (aInt > -1 & bInt > -1) Array[Int](aInt, bInt)
    else if (aInt > -1) Array[Int](aInt, eval(conflict.b).reduceLeft(_ max _))
    else if (bInt > -1) Array[Int](eval(conflict.a).reduceLeft(_ max _), bInt)
    else Array[Int](eval(conflict.a).reduceLeft(_ max _), eval(conflict.b).reduceLeft(_ max _))
  }

  /**
    * When there are 2 possible dads, we create a conflict.
    * @param prev The key of the conflict if we are already in one, null otherwise.
    * @return The key of the conflict.
    */
  def addConflict(prev: String): String = {
    var key = incCharacter()
    conflicts += key -> new Conflict("0", "0")

    if (prev != null) {
      var tokens = prev.split("_")

      var conflict = conflicts.get(tokens(0)).get
      if (tokens(1).equals("0")) conflict.a = key
      else conflict.b = key
    }
    key
  }

  /**
    * Update the value for one side of a conflict.
    * @param key The key of the conflict.
    * @param value The new value for the conflict.
    */
  def updateConflict(key: String, value: String): Unit = {

    var tokens = key.split("_")
    if(conflicts.get(tokens(0)).isEmpty) {
      println("empty");
    }
    var conflict = conflicts.get(tokens(0)).get

    if (tokens(1).equals("0")) conflict.a = value
    else conflict.b = value
  }

  //auto increase of the key (alphabetical character)
  private def incCharacter(): String = {
    var charValue = startCharacter.charAt(0)
    startCharacter = (charValue + 1).toChar.toString

    charValue.toString
  }

  /**
    * Get the value corresponding to the sum of all conflicts (the maximum for each conflict).
    * @return The value equal to the sum of all the highest value in each conflict.
    */
  def getValue(): Int = {
    var value = 0
    val keys = conflicts.keySet.toArray.sorted

    for (key <- keys) {
      if (conflicts.get(key).isDefined) {
        value += eval(key).reduceLeft(_ max _)
      }
    }

    value
  }

  //converter from string to int with error handling
  private def toInt(s: String): Int = {
    try {
      s.toInt
    } catch {
      case e: Exception => -1
    }
  }

  /**
    * Class representing a conflict.
    * @param val1 the value of the left part of the conflict.
    * @param val2 the value of the right part of the conflict.
    */
  class Conflict(val1: String, val2: String) {
    var a = val1
    var b = val2
  }

}
