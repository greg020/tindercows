package tools

import org.apache.commons.lang.StringEscapeUtils
import scala.collection.mutable

/**
  * DTO corresponding to the parsing of a bovine's card on the CDN website.
  * @param values results of the ExtractInfoParser.getInfos method.
  * @see ExtractInfoParser
  */
class WebParsingResult(values : mutable.HashMap[String,String]) {

  val id = {
    //We get it as "XXXXXX99999999"
    val elem = values.get("id")
    if (elem.isDefined) {
      //remove the alpha-numerical part
      //var d = elem.get.split("([A-Z]+[0-9]*[A-Z]+)", -1)
      //extract all the numbers
      var dList = ("""\d+""".r findAllIn elem.get).toList
      var d = dList(0)
      for (i <- 1 until dList.length) d += dList(i)
      for (c <- 0 until (8 - d.length)) d += "0"
      //assign each group of digits to the respective id
      (d.substring(0, 4).toInt, d.substring(4, d.length).toInt)
    } else (0,0)
  }

  val calving = {
    //we get it as "999"
    val elem = values.get("velage")
    if (elem.isDefined) CalvingDifficulty.get(elem.get.toInt) else null
  }

  val birthdate = {
    val date = values.get("naissance")
    if (date.isDefined) {
      //we transform to "Né le 99-AAA-99"
      var normalizedDate = StringEscapeUtils.unescapeHtml(date.get)
      var dateString = normalizedDate.split("le ")(1).trim
        val month = dateString.split("-")(1)
        val monthNumerical = month match {
          case "JAN" => "01"
          case "FÉB" | "FÉV" => "02"
          case "MAR" => "03"
          case "AVR" => "04"
          case "MAI" => "05"
          case "JUN" => "06"
          case "JUL" => "07"
          case "AOÛ" => "08"
          case "SEP" => "09"
          case "OCT" => "10"
          case "NOV" => "11"
          case "DÉC" => "12"
        }
        dateString = dateString.replace(month, monthNumerical)
        //we replace - by / to have the same format as Naissance.txt
        dateString = dateString.replace("-","/")
        dateString
    } else null
  }

  val longName = {
    val bName = values.get("nomLong")
    if (bName.isDefined) bName.get.trim else ""
  }

  val shortName = {
    val bName = values.get("nomCourt")
    if (bName.isDefined) bName.get.trim else ""
  }

  val somaticCells = {
    val cells = values.get("somatique")
    if (cells.isDefined) SomaticCellsConverter.SCStoSCC(cells.get.replace(",",".").toDouble) else 0
  }

  val mammalGland = {
    val mammal = values.get("mammaire")
    if (mammal.isDefined && mammal.get.matches("-?[0-9]+")) mammal.get.toInt else 0
  }

  val udder = {
    val udder = values.get("pis")
    if (udder.isDefined && udder.get.matches("-?[0-9]+")) udder.get.toInt else 0
  }

  val suspension = {
    val suspension = values.get("suspension")
    if (suspension.isDefined && suspension.get.matches("-?[0-9]+")) suspension.get.toInt else 0
  }

  val frontTeat = {
    val frontTeat = values.get("trayonAvant")
    if (frontTeat.isDefined && frontTeat.get.matches("-?[0-9]+")) frontTeat.get.toInt else 0
  }

  val backTeat = {
    val backTeat = values.get("trayonArriere")
    if (backTeat.isDefined && backTeat.get.matches("-?[0-9]+")) backTeat.get.toInt else 0
  }

  val foot = {
    val foot = values.get("pieds")
    if (foot.isDefined && foot.get.matches("-?[0-9]+")) foot.get.toInt else 0
  }

  val angle = {
    val angle = values.get("angle")
    if (angle.isDefined && angle.get.matches("-?[0-9]+")) angle.get.toInt else 0
  }

  val sideLimb = {
    val sideLimb = values.get("coteArriere")
    if (sideLimb.isDefined && sideLimb.get.matches("-?[0-9]+")) sideLimb.get.toInt else 0
  }

  val backLimb = {
    val backLimb = values.get("arriereArriere")
    if (backLimb.isDefined && backLimb.get.matches("-?[0-9]+")) backLimb.get.toInt else 0
  }

  val milk = {
    val milk = values.get("lait")
    if (milk.isDefined && milk.get.matches("-?[0-9]+")) milk.get.toInt else 0
  }

  val protein = {
    val protein = values.get("proteine")
    if (protein.isDefined && protein.get.matches("-?[0-9]+")) protein.get.toInt else 0
  }

  val fat = {
    val fat = values.get("gras")
    if (fat.isDefined && fat.get.matches("-?[0-9]+")) fat.get.toInt else 0
  }

  val fatImprovement = {
    val fatImprovement = values.get("grasAmelioration")
    if (fatImprovement.isDefined && fatImprovement.get.matches("-?[0-9]+,?")) fatImprovement.get.replace(",",".").toDouble else 0.0
  }

  val proteinImprovement = {
    val proteinImprovement = values.get("proteineAmelioration")
    if (proteinImprovement.isDefined && proteinImprovement.get.matches("-?[0-9]+,?")) proteinImprovement.get.replace(",",".").toDouble else 0.0
  }
}
