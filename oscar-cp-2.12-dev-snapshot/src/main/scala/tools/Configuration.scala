package tools

import Modelling.Selection

import scala.collection.mutable.ListBuffer

class Configuration {

  //Paths to create the graph
  var genealogyPath : String = null
  var samplePath : String = null
  var bullsIAPath : String = null
  var bullBreedingPath : String = null
  var conformationPath : String = null
  var lostNamesPath : String = null

  //Paths corresponding to the chosen bovine
  var cowsPath : String = null
  var bullsPath : String = null

  var nbMaxBulls : Int = 0

  var constraintsList : List[Constraint] = List()
  var sel: Selection = null

}

object Configuration {
  def apply(json: ConfigurationJSON): Configuration = {
    val config = new Configuration
    val listBuffer : ListBuffer[Constraint] = ListBuffer()
    config.genealogyPath = json.birthsPath
    config.samplePath = json.samplePath
    config.bullBreedingPath = json.bullBreedingPath
    config.conformationPath = json.conformationPath
    config.lostNamesPath = json.lostNamesPath

    config.cowsPath = json.chosenCowsPath
    config.bullsPath = json.chosenBullsPath
    config.nbMaxBulls = json.nbMaxBulls

    for(c <- json.constraints) {
      if (c.weight > 0) {
        //constraint is used
        listBuffer += Constraint(c)
      }
    }
    config.constraintsList = listBuffer.toList

    val sel = json.selections.filter(x => x.activated)
    if (sel.size > 0) config.sel = Selection(sel.head)
    config
  }
}
