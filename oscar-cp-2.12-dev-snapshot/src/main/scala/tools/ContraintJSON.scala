package tools

case class ConstraintJSON(name: String, weight: Int, isHard: Boolean, threshold: Double) {

}

case class SelectionJSON(name: String, activated: Boolean, from: String, to: String) {

}

case class ConfigurationJSON(birthsPath: String, bullBreedingPath: String, samplePath: String, conformationPath: String,
                             chosenCowsPath: String, chosenBullsPath: String,lostNamesPath: String, nbMaxBulls: Int,
                             constraints: List[ConstraintJSON], selections: List[SelectionJSON]) {
}
