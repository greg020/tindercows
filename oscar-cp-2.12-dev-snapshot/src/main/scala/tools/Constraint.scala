package tools

import tools.ConstraintName._

class Constraint(name: String, w: Int, isHard : Boolean, thrsld: Double) {
  val weight = w
  val hard = isHard
  val threshold = thrsld

  val ctrName = name.toLowerCase match {
      case "consanguinityworst" => CONSANGUINITY_WORST
      case "consanguinitythreshold" => CONSANGUINITY_THRESHOLD
      case "consanguinityavg" => CONSANGUINITY_AVG
      case "backteat" => BACK_TEAT
      case "backviewlimb" => BACK_VIEW_LIMB
      case "calving" => CALVING
      case "footangle" => FOOT_ANGLE
      case "frontteat" => FRONT_TEAT
      case "limbs" => LIMBS
      case "mammarygland" => MAMMARY_GLAND
      case "mediansuspension" => MEDIAN_SUSPENSION
      case "sideviewlimb" => SIDE_VIEW_LIMB
      case "somaticcells" => SOMATIC_CELLS
      case "udderdepth" => UDDER_DEPTH
      case "fatquality" => FAT_QUALITY
      case "proteinquality" => PROTEIN_QUALITY
      case "milkimprovement" => MILK_IMPROVEMENT
      case "fatimprovement" => FAT_IMPROVEMENT
      case "proteinimprovement" => PROTEIN_IMPROVEMENT
      case "teatlength" => TEAT_LENGTH
    }
}

object Constraint {

  def apply(ctrJson: ConstraintJSON): Constraint =
    new Constraint(ctrJson.name, ctrJson.weight, ctrJson.isHard, ctrJson.threshold)

}

