package tools

import java.net.URL

import org.htmlcleaner.{HtmlCleaner, TagNode}
import Modelling._

import util.control.Breaks._
import scala.collection.JavaConverters._
import scala.collection.mutable

/**
  * Object to call when you need to get genealogy and bull's informations from the CDN website.
  * The function getOnlineInfos is the one that do the job
  */
object BullWebParser {

  //list of bovine's sex in order got from parser
  private val nodeList = Array("M", "F", "M", "M", "F", "F", "M", "F", "M", "M", "F", "F", "M", "F")

  /**
    * Return an array of TagNode corresponding to a class, whether it is the same or only contains the given keyword.
    * @param elements The original array of node (corresponding to the webpage parsed).
    * @param className Keyword given in order to filter the nodes belonging to a certain css class.
    * @param mustBeEqual A value to know if the class must be the same as the keyword or contain it.
    * @return Array of node that fulfill the className.
    */
  private def extractNode(elements: Array[TagNode], className: String, mustBeEqual: Boolean): Array[TagNode] = {
    var array: Array[TagNode] = Array()
    breakable {
      for (elem <- elements) {
        val classType = elem.getAttributeByName("class")
        if (classType != null &&
          ((mustBeEqual && classType.equalsIgnoreCase(className))
            || (!mustBeEqual && classType.contains(className)))) {
          array :+= elem
        }
      }
    }
    array
  }

  /**
    * Get the TagNodes corresponding to the genealogy of the bull we want to know more about.
    * @param url It is the url to the summary card of the bull
    * @return Array with the genealogy of the bull in order described above.
    */
  private def extractGenealogy(url : String) : Array[TagNode] = {
    val urlM = url.replace("ge", "pedigree")
    val cleaner = new HtmlCleaner
    val rootNode = cleaner.clean(new URL(urlM))

    //get the genealogy
    var elements = rootNode.getElementsByName("td", true)
    var element: TagNode = null
    //get the parents
    var array = extractNode(elements, "animalUnderline", true)
    //get the rest of the genealogy
    elements = rootNode.getElementsByName("td", true)
    element = null
    array = array ++ (extractNode(elements, "animalSmallText", false))

    array
  }

  /**
    * Transform genealogy's TagNode informations in Bull objects.
    * @param nodeArray Array containing the genealogy as TagNode
    * @return Array containing the genealogy as Bovine
    */
  private def createBovineGenealogy(nodeArray : Array[TagNode]) : Array[Bovine] = {
    var bovArray : Array[Bovine] = Array()
    //for each node get infos
    for (i <- 0 until nodeArray.length) {
      var infos = nodeArray(i).getChildTagList.asScala

      val pos = (0 until infos.size).filter(infos(_)
        .isInstanceOf[TagNode]).filter(infos(_).asInstanceOf[TagNode].getName.equalsIgnoreCase("a"))(0)

      val result = ExtractInfoParser.getInfos("https://www.cdn.ca/francais/query/" +
        infos(pos).asInstanceOf[TagNode].getAttributes.get("href"))

      var bov = if (nodeList(i).equals("M")) Bull(result) else Cow(result)
      bovArray = bovArray :+ bov
    }

    bovArray
  }

  /**
    * Add the bovine in their respectable hashmap while being converted to the correct type & linked between them.
    * @param bov Array containing all the bovine from the genealogy
    * @param cows Hashmap containing all the cows of the herd
    * @param bulls Hashmap containing all the bulls of the herd
    */
  private def addToHerd(bov : Array[Bovine], cows : mutable.HashMap[(Int,Int), Cow], bulls : mutable.HashMap[String,Bull]) : Unit = {
    //parent
    bov(0).fathers(0) = bov(1).asInstanceOf[Bull]
    bov(0).mother = bov(2).asInstanceOf[Cow]
    //grandparents
    bov(1).fathers(0) = bov(3).asInstanceOf[Bull]
    bov(1).mother = bov(6).asInstanceOf[Cow]
    bov(2).fathers(0) = bov(9).asInstanceOf[Bull]
    bov(2).mother = bov(12).asInstanceOf[Cow]
    //great-grandparents
    bov(3).fathers(0) = bov(4).asInstanceOf[Bull]
    bov(3).mother = bov(5).asInstanceOf[Cow]
    bov(6).fathers(0) = bov(7).asInstanceOf[Bull]
    bov(6).mother = bov(8).asInstanceOf[Cow]
    bov(9).fathers(0) = bov(10).asInstanceOf[Bull]
    bov(9).mother = bov(11).asInstanceOf[Cow]
    bov(12).fathers(0) = bov(13).asInstanceOf[Bull]
    bov(12).mother = bov(14).asInstanceOf[Cow]
    //add them all to the correct map
    bulls += bov(0).name -> bov(0).asInstanceOf[Bull]
    for (i <- 1 until bov.length) {
      if (nodeList(i-1).equals("M"))
        bulls += bov(i).name -> bov(i).asInstanceOf[Bull]
      else
        cows += (bov(i).idA,bov(i).idB) -> bov(i).asInstanceOf[Cow]
    }
  }

  /**
    * Method to retrieve online information about a bovine based on cdn's url.
    * Call to this method will add the genealogy of the bovine in the data.
    * Each bovine added will have it's informations completed with everything available online.
    * @param url Link to the bovine's data on the cdn website
    */
  def getOnlineInfos(url: String, cows: mutable.HashMap[(Int, Int), Cow], bulls: mutable.HashMap[String, Bull]): Unit = {

    val cleaner = new HtmlCleaner
    //get the bull infos
    val result = ExtractInfoParser.getInfos(url)
    val bull = Bull(result)

    //get the genealogy
    var nodeArray = extractGenealogy(url)
    //convert it to bovine objects
    var bovineArray = createBovineGenealogy(nodeArray)
    //add the first bull to the rest of the genealogy
    var cmpleteArray = Array(bull) ++ bovineArray
    //create links and add to the herd
    addToHerd(cmpleteArray,cows,bulls)
  }
}
