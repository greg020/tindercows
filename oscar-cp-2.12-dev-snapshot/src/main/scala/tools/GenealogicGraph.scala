package tools

import Modelling.{Bovine, Bull, Cow}

import scala.collection.mutable

@SerialVersionUID(10L)
class GenealogicGraph(var cows: mutable.HashMap[(Int, Int), Cow], var bulls: mutable.HashMap[String, Bull]) extends Serializable {

  override def toString: String = {
    var sb = new mutable.StringBuilder()
    var queue = new mutable.Queue[Bovine]()

    for(key <- cows.keys) {
      queue += cows.get(key).get

      while(!queue.isEmpty) {
        val node = queue.dequeue()
        sb = sb.append(node.toString)
        if(node.mother != null) queue += node.mother
        for (father <- node.fathers) {
          if (father != null) queue += father
        }
      }
      sb = sb.append("\n")
    }

    for(key <- bulls.keys) {
      queue += bulls.get(key).get

      while(!queue.isEmpty) {
        val node = queue.dequeue()
        sb = sb.append(node.toString)
        if(node.mother != null) queue += node.mother
        for (father <- node.fathers) {
          if (father != null) queue += father
        }
      }
      sb = sb.append("\n")
    }

    return sb.result()
  }

}
