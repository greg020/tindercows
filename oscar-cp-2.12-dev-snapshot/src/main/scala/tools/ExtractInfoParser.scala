package tools

import java.net.URL
import org.apache.commons.lang.StringEscapeUtils
import org.htmlcleaner.{HtmlCleaner, TagNode}
import scala.collection.mutable
import scala.util.control.Breaks.{break, breakable}

/**
  * Object extracting information from the CDN website.
  */
object ExtractInfoParser {

  private var classMap: mutable.HashMap[String, KeyWordInfo] = null
  private var elemMap: mutable.HashMap[String, KeyWordInfo] = null
  private var result: mutable.HashMap[String, String] = null

  /**
    * Get all the informations needed from a bovine's card on the cdn website.
    * @param url The url of the bovine's summary card.
    * @return A DTO containing the values of the bovine's card.
    * @see WebParsingResult
    */
  def getInfos(url : String): WebParsingResult = {
    classMap = mutable.HashMap[String, KeyWordInfo]()
    elemMap = mutable.HashMap[String, KeyWordInfo]()
    result = mutable.HashMap[String, String]()

    //encode the informations encapsulated in a class on the website here
    //the steps is the number of tag of the same class we need to go through to have our information
    classMap += "id" -> new KeyWordInfo("animalHeaderSubTitle", 0, true)
    classMap += "nomLong" -> new KeyWordInfo("animalHeaderSubTitle", 1, true)
    classMap += "nomCourt" -> new KeyWordInfo("animalHeaderSubTitle", 2, true)
    //same with values inside a tag
    //here the steps is the number of tag we need to ignore before finding the information when we found the keyword
    elemMap += "mammaire" -> new KeyWordInfo(StringEscapeUtils.escapeHtml("Système mammaire"), 1, true)
    elemMap += "pis" -> new KeyWordInfo(StringEscapeUtils.escapeHtml("Profondeur du pis"), 1, true)
    elemMap += "suspension" -> new KeyWordInfo(StringEscapeUtils.escapeHtml("Suspension médiane"), 1, true)
    elemMap += "trayonAvant" -> new KeyWordInfo(StringEscapeUtils.escapeHtml("Position trayons avant"), 1, true)
    elemMap += "trayonArriere" -> new KeyWordInfo(StringEscapeUtils.escapeHtml("Position trayons arrière"), 1, true)
    elemMap += "pieds" -> new KeyWordInfo(StringEscapeUtils.escapeHtml("Pieds et membres"), 1, true)
    elemMap += "angle" -> new KeyWordInfo(StringEscapeUtils.escapeHtml("Angle du pied"), 1, true)
    elemMap += "coteArriere" -> new KeyWordInfo(StringEscapeUtils.escapeHtml("Vue côté-membres arrière"), 1, true)
    elemMap += "arriereArriere" -> new KeyWordInfo(StringEscapeUtils.escapeHtml("Vue arrière-membres arrière"), 1, true)
    elemMap += "lait" -> new KeyWordInfo(StringEscapeUtils.escapeHtml("Lait"), 1, true)
    elemMap += "gras" -> new KeyWordInfo(StringEscapeUtils.escapeHtml("Gras"), 1, true)
    elemMap += "proteine" -> new KeyWordInfo(StringEscapeUtils.escapeHtml("Protéine"), 1, true)
    elemMap += "grasAmelioration" -> new KeyWordInfo(StringEscapeUtils.escapeHtml("Gras"), 3, true)
    elemMap += "proteineAmelioration" -> new KeyWordInfo(StringEscapeUtils.escapeHtml("Protéine"), 3, true)
    elemMap += "velage" -> new KeyWordInfo(StringEscapeUtils.escapeHtml("Aptitude au vêlage"), 1, true)
    elemMap += "naissance" -> new KeyWordInfo(StringEscapeUtils.escapeHtml("Né"),0, false)
    elemMap += "somatique" -> new KeyWordInfo(StringEscapeUtils.escapeHtml("Cellules somatiques"), 1, true)

    val cleaner = new HtmlCleaner
    val rootNode = cleaner.clean(new URL(url))
    //clean the elements that we can ignore
    var elements = rootNode.getElementsByName("td", true).filter(_.hasChildren).filter(!_.getChildren.get(0).toString.equals("&nbsp;"))
    //.filter(!_.getChildren.get(0).toString.equals("&nbsp;"))

    breakable {
      for(elem <- elements) {
        contact(elem)
        if (isEmpty()) break
      }
    }

    new WebParsingResult(result)
  }

  /**
    * Gives node the class & element filtering method if there are tags left.
    * @param node The node we want to analyze
    */
  private def contact(node: TagNode) : Unit = {
    if (!classMap.isEmpty) classContact(node)
    if (!elemMap.isEmpty) elemContact(node)
  }

  /**
    * Check if the node corresponds to a keyword regarding the class parsing.
    * @param node The node we want to analyze.
    */
  private def classContact(node: TagNode) : Unit = {
    val classType = node.getAttributeByName("class")
    if(classType != null) {
      for(key <- classMap.keys) {
        var elem = classMap.get(key)
        if (elem.isDefined && ((elem.get.mustBeEqual && classType.equalsIgnoreCase(elem.get.text))
          || (!elem.get.mustBeEqual && classType.contains(elem.get.text)))) {
          //first encounter
          if(elem.get.curr_step == -1) elem.get.curr_step = elem.get.nbSteps
          //bingo that's the node we want
          if(elem.get.curr_step == 0) {
            //add the text to result
            result += key -> node.getChildren.get(0).toString
            //remove the search from map
            classMap -= key
          }
          //already matched class but not the one that we want
          if(elem.get.curr_step > 0) elem.get.curr_step -= 1
        }
      }
    }
  }

  /**
    * Check if the node corresponds to a keyword regarding the element's text parsing.
    * @param node The node we want to analyze.
    */
  private def elemContact(node: TagNode): Unit = {
    for(key <- elemMap.keys) {
      var elem = elemMap.get(key)

      if(elem.isDefined &&
        ((elem.get.mustBeEqual && node.getChildren.get(0).toString.equalsIgnoreCase(elem.get.text))
          || (!elem.get.mustBeEqual && node.getChildren.get(0).toString.contains(elem.get.text)))) {
        elem.get.curr_step = elem.get.nbSteps
      }
      //bingo that's the node we want
      if(elem.get.curr_step == 0) {
        //add the text to result
        result += key -> node.getChildren.get(0).toString
        //remove the search from map
        elemMap -= key
      }
      //already matched elem before and trying to reach the value we want
      if(elem.get.curr_step > 0) elem.get.curr_step -= 1
    }
  }

  /**
    * Check if there are still search keywords available.
    * @return true if there is still at least one keyword to search, false otherwise.
    */
  private def isEmpty() : Boolean = {
    classMap.isEmpty && elemMap.isEmpty
  }

  /**
    * Class corresponding to a search.
    * @param keyword The keyword we want to identify during parsing.
    * @param steps Number of steps we need to ignore before finding the result we are interested in.
    * @param beEquals Whether the element must be equal to the keyword or contains it.
    */
  class KeyWordInfo(keyword: String, steps: Int, beEquals: Boolean) {
    var curr_step = -1
    val text = keyword
    val nbSteps = steps
    val mustBeEqual = beEquals
  }
}
