package Main

import java.io.{FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream}
import java.nio.file.{Files, Paths}

import tools.GenealogicGraph

object webParser extends App {

  var graph : GenealogicGraph = null
  if (Files.exists(Paths.get("data/webGraph"))) {
    val ois = new ObjectInputStream(new FileInputStream("data/webGraph"))
    graph = ois.readObject.asInstanceOf[GenealogicGraph]
    ois.close
    HandleFile.female = graph.cows
    HandleFile.male = graph.bulls
    println(graph.bulls.size)
  }

  /*HandleFile.handleWebParser("data/ia.txt")
  graph = new GenealogicGraph(HandleFile.female, HandleFile.male)

  val oos = new ObjectOutputStream(new FileOutputStream("data/webGraph"))
  oos.writeObject(graph)
  oos.close()*/



}
