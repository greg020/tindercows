package Main

import java.io._
import java.nio.file.{Files, Paths}

import Modelling.Consanguinity
import net.liftweb.json.{DefaultFormats, parse}
import tools.{Configuration, ConfigurationJSON, GenealogicGraph}

import scala.io.Source
import scala.io.Source.fromFile

object ScalabilityTest extends App {
  implicit val formats = DefaultFormats

  val text = Source.fromFile("data/config.json").mkString

  val json = parse(text)
  val config = json.extract[ConfigurationJSON]

  val pgmConfig = Configuration(config)

  val N = args(0).toInt //number of cows
  val K = args(1).toInt //number of available bulls
  val M = args(2).toInt //maximal number of Bulls atMostNValue

  var graph : GenealogicGraph = null

  if (Files.exists(Paths.get("data/dataGraph"))) {
    val ois = new ObjectInputStream(new FileInputStream("data/dataGraph"))
    graph = ois.readObject.asInstanceOf[GenealogicGraph]
    ois.close
  }

  /*val oos = new ObjectOutputStream(new FileOutputStream("data/graph"))
  oos.writeObject(graph)
  oos.close*/

  HandleFile.female = graph.cows
  HandleFile.male = graph.bulls

  var cowList = fromFile("data/cows.txt").getLines().toList
  var bullList = fromFile("data/bulls.txt").getLines().toList.map(x => x.split("\t")).map(xs => xs(0).toLowerCase)

  cowList = scala.util.Random.shuffle(cowList)
  bullList = scala.util.Random.shuffle(bullList)

  var cowTestList = cowList.slice(0, N)
  var bullTestList = generateList(bullList, K, graph)

  println("Nb cows : "+N+", nb bulls : "+K+", nb max bulls : "+M)
  pgmConfig.nbMaxBulls = M
  val start = System.currentTimeMillis()
  Cows.startModel(pgmConfig,cowTestList, bullTestList, graph)
  val stop = System.currentTimeMillis()
  println("Time : "+(stop-start)+"ms")
  println("********************************************")

  private def generateCowsFile() : Unit = {
    var cows = List[String]()
    for (line <- fromFile("data/Naissances_v2.txt", "iso-8859-1").getLines().map(_.split("\t", -1))) {
      val vealIdA = line(9)
      // line(10) = idB of the veal
      val vealIdB = line(10)
      // line(14) = sex of the veal
      val sex = line(14)
      // line(15) = status of the veal (born dead, dead, sold or no information)
      val status = line(15)
      if (!vealIdA.equalsIgnoreCase("")) {
        if (!status.equals("MN")) {
          if (sex.equals("F")) {
            cows :+= (vealIdA+"\t"+vealIdB)
          }
        }
      }
    }

    val output = new File("data/cows.txt")
    val writer = new FileWriter(output)

    for(line <- cows) writer.write(line+"\n")
    writer.close()
  }

  private def generateList(list : List[String], nb : Int, graph : GenealogicGraph) : List[String] = {

    var correctList = List[String]()
    var cpt = 0
    var pos = 0

    while(pos < list.size && cpt < nb) {
      if (graph.bulls.getOrElse(list(pos), null) != null) {
        correctList :+= list(pos)
        cpt += 1
      }
      pos += 1
    }

    correctList
  }
}
