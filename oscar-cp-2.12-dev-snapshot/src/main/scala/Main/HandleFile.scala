package Main

import java.io.IOException

import Modelling.{Bovine, Bull, Cow, Selection}
import tools.{BullWebParser, GenealogicGraph}

import scala.collection.mutable
import scala.io.Source._

object HandleFile {

  // data structure to remember the cows that we have already seen
  var female: mutable.HashMap[(Int, Int), Cow] = new mutable.HashMap[(Int, Int), Cow]()
  var male: mutable.HashMap[String, Bull] = new mutable.HashMap[String, Bull]()
  var cowSet : mutable.HashSet[(Int,Int)] = new mutable.HashSet[(Int, Int)]()

  def handleFile(fileName: String, map: mutable.HashMap[(Int, Int), String] = null): Unit = {

    for (line <- fromFile(fileName, "iso-8859-1").getLines().map(_.split("\t", -1))) {
      // line(0) = number of the birth line in the file. We need it to see if we deal with a multiple birth
      val birthline = line(0)
      // line(1) = idA of the mother
      val motherIdA = line(1).toInt
      // line(2) = idB of the mother
      val motherIdB = line(2).toInt
      // line(3) = number of the birth line corresponding to the previous vealing of the mother(/ if none)
      // line(4) = number of vealing of the mother (at least 1)
      // line(5) = number of the birth line corresponding to the next vealing of the mother (/ if none)
      // line(6) = name of the mother
      val motherName = line(6).toLowerCase

      // line(7) = name of the father (could be two of the form father1/father2)
      val fatherName = line(7).toLowerCase

      // line(8) = description in french of the birth
      // line(9) = idA of the veal
      val vealIdA = line(9)
      // line(10) = idB of the veal
      val vealIdB = line(10)
      // line(11) = name if the veal
      var vealName = line(11).toLowerCase
      // line(12) = color of the veal
      val color = line(12)
      // line(13) = birthdate
      val birthdate = line(13)
      // line(14) = sex of the veal
      val sex = line(14)
      // line(15) = status of the veal (born dead, dead, sold or no information)
      val status = line(15)

      // line(16) = number of the birth line corresponding of the first vealing of the veal, if it's a female

      // line(17) = difficult vealing? (boolean value, 1 or nothing)
      val difVealing = line(17)

      // ---------------------------------
      // Deal with the mother. Add her to the known mothers, fix her name if unknown, count births and bad births.
      var mother = female.getOrElse((motherIdA, motherIdB), new Cow(motherIdA, motherIdB, motherName))
      // handle only once the multiple birth
      if (birthline.contains("a") || !birthline.matches(".*[A-z]$")) {
        mother.nbCalving = mother.nbCalving + 1
        if (difVealing.equals("1")) mother.nbBadCalving = mother.nbBadCalving + 1
      }
      // unpdate the mother's name if not previously known.
      if (mother.name.equals("") && !motherName.equals("")) mother.name = motherName
      female.update((motherIdA, motherIdB), mother)


      // ----------------------------
      // handles the veal (the dad case is handled here too
      // we don't deal with unknown id for the veal
      if (!vealIdA.equalsIgnoreCase("")) {
        if (!status.equals("MN")) {
          var veal: Bovine = new Bovine
          if (vealIdA.equalsIgnoreCase("")) println(birthline)
          if (sex.equals("F")) {
            veal = female.getOrElseUpdate((vealIdA.toInt, vealIdB.toInt), new Cow(vealIdA.toInt, vealIdB.toInt, vealName))
          } else if (sex.equals("M") && !vealName.equals("")) {
            veal = male.getOrElseUpdate((vealName), new Bull(vealName))
            if (vealIdA.matches("[0-9]*") && vealIdB.matches("[0-9]*")) {
              veal.idA = vealIdA.toInt
              veal.idB = vealIdB.toInt

              if (map != null && map.contains((vealIdA.toInt,vealIdB.toInt))) vealName = map.get((vealIdA.toInt,vealIdB.toInt)).get
            }
          } // last case: sex = "?", usually the veal is dead or sold straigth away. We don't deal with that.

          veal.mother = mother
          handleFathers(veal, fatherName)
          veal.birthdate = birthdate
          veal.color = color

          if (sex.equals("F")) {
            female.update((vealIdA.toInt, vealIdB.toInt), veal.asInstanceOf[Cow])
            cowSet.add(veal.idA, veal.idB)

          } else if (sex.equals("M") && !vealName.equals("")) {
            male.update((vealName), veal.asInstanceOf[Bull])
          }
        }
      }

    }
  }

  def handleFathers(veal: => Bovine, fatherName: String): Unit = {
    //multiple cases for father name failure
    val lowerCaseName = fatherName.toLowerCase
    if (!lowerCaseName.equals("") && !lowerCaseName.matches("\\?+") && !lowerCaseName.matches("X+")) {
      if (lowerCaseName.contains("/")) {
        val fathers = lowerCaseName.split("/")
        val f1 = male.getOrElseUpdate(fathers(0), new Bull(fathers(0)))
        val f2 = male.getOrElseUpdate(fathers(1), new Bull(fathers(1)))
        veal.fathers(0) = f1
        veal.fathers(1) = f2
      } else {
        val f = male.getOrElseUpdate(lowerCaseName, new Bull(lowerCaseName))
        veal.fathers(0) = f
      }
    }
  }

  def initGraph(): GenealogicGraph = {
    handleFile("data/Naissances_v2.txt")
    handleBullsFile("data/TaureauxElevage.txt")
    handleCells("data/sample.txt")
    //handleWebParser("data/taureauxIA.txt")
    val graph = new GenealogicGraph(female, male)
    graph
  }

  def handleBullsFile(fileName: String): Unit = {

    val lines = fromFile(fileName, "iso-8859-1").getLines().toList
    val ghostIdPrefix = 0
    var ghostIdSuffix = 1

    for (i <- 1 until lines.size) {
      var line = lines(i).split("\t", -1)
      //line(0) is the bull we want to know the genealogy
      var currentBovine: Bovine = male.getOrElseUpdate(line(0), new Bull(line(0)))

      //line(x) is a possible father
      for (j <- 1 until line.size) {
        //Is there a name ?
        if (!line(j).equalsIgnoreCase("?")) {
          //Is there multiple names ?
          var dadNames = line(j).split("/")

          var dad = male.getOrElseUpdate(dadNames(0), new Bull(dadNames(0)))
          currentBovine.fathers(0) = dad
          //2 dads case
          if (dadNames.length > 1) {
            var secDaddy = male.getOrElseUpdate(dadNames(1), new Bull(dadNames(1)))
            currentBovine.fathers(1) = secDaddy
          }
        }
        //check if the bull already has a mother
        var tmpMom = if (currentBovine.mother == null) null else female.get(currentBovine.mother.idA,currentBovine.mother.idB).get
        if (tmpMom == null) {
          //if no mom has been found
          //create a ghost cow to establish a genealogy link even if we don't know the mother
          tmpMom = new Cow(ghostIdPrefix, ghostIdSuffix, nName = "X")
          female += (tmpMom.idA, tmpMom.idB) -> tmpMom
          //increment de ghost id
          ghostIdSuffix += 1
          currentBovine.mother = tmpMom
        }
        //set the ghost mom as the current bovine
        currentBovine = tmpMom
      }
    }
  }

  def handleCells(fileName: String): Unit = {
    val lines = fromFile(fileName, "iso-8859-1").getLines().toList
    val cowSampleSet : mutable.HashSet[(Int,Int)] = new mutable.HashSet[(Int, Int)]()

    for (i <- 0 until lines.size) {
      val line = lines(i).split("\t", -1)
      //line(0) & line(1)
      val idA = line(0).toInt
      val idB = line(1).toInt

      cowSampleSet.add(idA,idB)

      val currentCow : Cow = female.getOrElse((idA, idB), null)

      if (currentCow != null) {
        //line(2) -> tCell
        val cell = line(2).toInt
        currentCow.somaticCells = cell
        //line(3) -> tMG, convert string to double
        var mg = line(3).replace(",",".").toDouble
        currentCow.fatTotal += mg
        currentCow.fatNbMeasurements += 1
        //line(4) -> tPR, convert string to double
        var pr = line(4).replace(",",".").toDouble
        currentCow.proteinTotal += pr
        currentCow.proteinNbMeasurements += 1
      }
    }

    //compute mean for each cow and the general mean
    var totalFat : Double = 0
    var totalProtein : Double = 0

    for (ids <- cowSampleSet) {
      totalFat += (if (female.getOrElse((ids._1,ids._2), null) == null) 0 else  female.get(ids._1,ids._2).get.computeFatMean())
      totalProtein += (if (female.getOrElse((ids._1, ids._2), null) == null) 0 else female.get(ids._1,ids._2).get.computeProteinMean())
    }

    var meanFat = totalFat / cowSampleSet.size
    var meanProtein = totalProtein / cowSampleSet.size

    Cow.MEAN_FAT = meanFat
    Cow.MEAN_PROTEIN = meanProtein

    //get the cows withtout measurements
    var noMeasurementCows = cowSet.diff(cowSampleSet)

    for (ids <- noMeasurementCows) {
      female.getOrElse((ids._1, ids._2), null).protein = meanProtein
      female.getOrElse((ids._1, ids._2), null).fat = meanFat
    }
  }

  def handleWebParser(fileName: String): Unit = {
    var lines = fromFile(fileName, "iso-8859-1").getLines().toList

    for (i <- 0 until lines.size) {
      println(i)
      var line = lines(i).split("\t")

      if (line.length >= 2 && !line(2).isEmpty) {
        try {
          BullWebParser.getOnlineInfos(line(2), female, male)
        } catch {
          case e : IOException => {
            male.remove(line(1))
            println("Problem line : "+i)
          }
        }
      }
    }
  }

  def handleLostName(fileName: String): mutable.HashMap[(Int, Int), String] = {
    var lines = fromFile(fileName, "iso-8859-1").getLines().toList
    val map = mutable.HashMap[(Int, Int), String]()

    for (i <- 0 until lines.size) {
      val line = lines(i).split("\t", -1)

      val name = line(0)
      val idA = line(1).toInt
      val idB = line(2).toInt

      if (idA != -2) map += (idA, idB) -> name
    }

    map
  }

  def handleConformation(fileName: String): Unit = {
    var lines = fromFile(fileName, "iso-8859-1").getLines().toList

    for (i <- 0 until lines.size) {
      val line = lines(i).split("\t", -1)

      //"XXXXa\tXXXXb
      val idA = line(0).toInt
      val idB = line(1).toInt
      //Date
      //SystemeMamaire
      val mammalSystem = if(line(3) != "") line(3).toInt else 0
      //ProfondeurPis
      val udderDepth = if(line(4) != "") line(4).toInt else 0
      //SuspensionMediane
      val medianSuspension = if(line(5) != "") line(5).toInt else 0
      //PositionTrayonsAvant
      val frontTeat = if(line(6) != "") line(6).toInt else 0
      //PositionTrayonsArrière
      val backTeat = if(line(7) != "") line(7).toInt else 0
      //LongeurTrayons
      val teatLength = if(line(8) != "") line(8).toInt else 0
      //PiedsEtMembres
      val limbs = if(line(9) != "") line(9).toInt else 0
      //AngleDuPied
      val footAngle = if(line(10) != "") line(10).toInt else 0
      //VueCotéMembreArrière
      val sideViewLimb = if(line(11) != "") line(11).toInt else 0
      //VueArrièreMembreArrière
      val backViewLimb = if(line(12) != "") line(12).toInt else 0

      val cow = female.get((idA,idB)).get
      cow.mammaryGland = mammalSystem
      cow.udderDepth = udderDepth
      cow.medianSuspension = medianSuspension
      cow.frontTeat = frontTeat
      cow.backTeat = backTeat
      cow.teatLength = teatLength
      cow.limbs = limbs
      cow.footAngle = footAngle
      cow.sideViewLimb = sideViewLimb
      cow.backViewLimb = backViewLimb

      female.update((idA,idB), cow)
    }

  }

  def handleSelection(ids: List[(Int, Int)], sel: Selection) : List[(Int,Int)] = {
    ids.filter(x => sel.isAcceptable(female.get(x).get))
  }

}
