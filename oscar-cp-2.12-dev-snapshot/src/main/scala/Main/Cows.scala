package Main

import java.io.{FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream}
import java.nio.file.{Files, Paths}

import Modelling._
import oscar.cp.constraints.AtMostNValue
import oscar.cp.{CPModel, _}
import tools.{Configuration, GenealogicGraph}

import scala.io.Source._
import tools.ConstraintName._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object Cows extends CPModel {

  def correctList(list: List[String], graph : GenealogicGraph, isCows : Boolean) : (List[String], List[String]) = {

    var correctList = List[String]()
    var incorrectList = List[String]()

    if (isCows) {
      for(line <- list) {
        var momKey = line.split("\t")
        val idA = momKey(0).toInt
        val idB = momKey(1).toInt

        if (graph.cows.getOrElse((idA, idB), null) == null)
          incorrectList :+= line
        else
          correctList :+= line
      }
    } else {
      for(line <- list) {
        if (graph.bulls.getOrElse(line, null) == null)
          incorrectList :+= line
        else
          correctList :+= line
      }
    }

    (correctList, incorrectList)
  }

  def startModel(config : Configuration, cowList: List[String], bullList: List[String], graph: GenealogicGraph): Unit = {

    //Restore the save of the IA bulls
    /*var graph : GenealogicGraph = null
    if (Files.exists(Paths.get("data/dataGraph"))) {
      val ois = new ObjectInputStream(new FileInputStream("data/dataGraph"))
      graph = ois.readObject.asInstanceOf[GenealogicGraph]
      ois.close
    }

    HandleFile.female = graph.cows
    HandleFile.male = graph.bulls*/

    //pre compute lost name for the bulls
    /*val lostNames = HandleFile.handleLostName(config.lostNamesPath)
    //initialize the graph
    HandleFile.handleFile(config.genealogyPath, lostNames)
    // add bulls genealogy
    HandleFile.handleBullsFile(config.bullBreedingPath)
    // add cells, fat and protein infos
    HandleFile.handleCells(config.samplePath)
    // add conformation infos
    HandleFile.handleConformation(config.conformationPath)
    //create the graph
    graph = new GenealogicGraph(HandleFile.female, HandleFile.male)*/
    //set all ids to a list
    var cowIds = cowList//fromFile(config.cowsPath, "iso-8859-1").getLines().toList
    var bullIds = bullList//fromFile(config.bullsPath, "iso-8859-1").getLines().toList
    //check if there is any error with the ids
    val correctsCowList = correctList(cowIds, graph, true)
    val correctsBullList = correctList(bullIds, graph, false)
    //assign the lists without error
    cowIds = correctsCowList._1
    bullIds = correctsBullList._1

    HandleFile.female = graph.cows
    HandleFile.male = graph.bulls

    //parse ids to (Int, Int)

    //TEST AREA --------------------------------//
    /*var graph : GenealogicGraph = null

    val marg = new Cow(1,1, "marg")
    val orchi = new Cow(1,2, "orchi")
    val rose = new Cow(1,3, "rose")

    val bruno = new Bull("bruno")
    val trump = new Bull("trump")
    val achille = new Bull("achille")

    bruno.milk = 300
    bruno.proteinImprovement = 0.3
    trump.milk = 500
    trump.proteinImprovement = 0.2

    var female = new mutable.HashMap[(Int,Int), Cow]()
    female += ((1,1) -> marg, (1,2) -> orchi, (1,3) -> rose)
    var male = new mutable.HashMap[String, Bull]()
    male += ("bruno" -> bruno, "achille" -> achille, "trump" -> trump)

    graph = new GenealogicGraph(female, male)

    var cowIds = List("1 1", "1 2")
    var bullIds = List("bruno", "trump")*/

    //END OF TEST DATA ------------------------//
    var parsedCowIds : List[(Int, Int)] = cowIds.map(x => x.split("\t")).map(y => (y(0).toInt, y(1).toInt))
    //check if there is a selection
    if (config.sel != null) parsedCowIds = HandleFile.handleSelection(parsedCowIds, config.sel)

    //initialize fixed variables
    val nbCows = cowIds.size
    val nbBulls = bullIds.size
    val nbConstraint = config.constraintsList.size
    var nbMaxBulls = CPIntVar(config.nbMaxBulls)


    //create the domain of bulls for each cows
    val bullForCows : Array[CPIntVar] = Array.ofDim(nbCows)
    //initialize the domain of each cow with all the bulls
    for (cow <- 0 until nbCows) bullForCows(cow) = CPIntVar(0 until nbBulls)
    //create a container for all cpintvar
    var constrainedVariables = Seq[CPIntVar]()
    //create a vector with all the weights corresponding to the constraints
    val constraintWeights = Array.ofDim[Int](nbConstraint)
    for (i <- 0 until nbConstraint) constraintWeights(i) = config.constraintsList(i).weight

    //create a cost matrix for all the cows, the bulls and all the constraints
    val costMatrix = Array.ofDim[Int](nbConstraint, nbCows, nbBulls)
    var costs = Array.ofDim[CPIntVar](nbConstraint,nbCows)
    var totalCost : Array[CPIntVar] = Array.ofDim(nbConstraint)


    //LET'S THE GIGALOOP FOR ALL THE CONSTRAINTS BEGINS
    for (cow <- 0 until nbCows) {
      //iterate over all the bulls
      for (bull <- 0 until nbBulls) {
        //and now iterate over all the constraints
        for(j <- 0 until nbConstraint) {

          val penalty = config.constraintsList(j).ctrName match {
            case CONSANGUINITY_THRESHOLD  => {
              val result = Consanguinity.consanguinity(graph.cows.getOrElse(parsedCowIds(cow), null),
                graph.bulls.getOrElse(bullIds(bull), null))
              if (result > config.constraintsList(j).threshold) Bovine.SENTINEL_VALUE else result
            }
            case CONSANGUINITY_AVG | CONSANGUINITY_WORST => Consanguinity.consanguinity(graph.cows.getOrElse(
              parsedCowIds(cow), null),graph.bulls.getOrElse(bullIds(bull), null))
            case BACK_TEAT => BackTeat.computePenalty(graph.cows.getOrElse(parsedCowIds(cow), null),
              graph.bulls.getOrElse(bullIds(bull), null), config.constraintsList(j).hard)
            case BACK_VIEW_LIMB => BackViewLimb.computePenalty(graph.cows.getOrElse(parsedCowIds(cow), null),
              graph.bulls.getOrElse(bullIds(bull), null), config.constraintsList(j).hard)
            case CALVING => Calving.difficultyScore(graph.cows.getOrElse(parsedCowIds(cow), null),
              graph.bulls.getOrElse(bullIds(bull), null))
            case FAT_QUALITY => Fat.computePenalty(graph.cows.getOrElse(parsedCowIds(cow), null),
              graph.bulls.getOrElse(bullIds(bull), null), config.constraintsList(j).hard)
            case FAT_IMPROVEMENT => Fat.computeImprovement(graph.bulls.getOrElse(bullIds(bull), null))
            case FOOT_ANGLE => FootAngle.computePenalty(graph.cows.getOrElse(parsedCowIds(cow), null),
              graph.bulls.getOrElse(bullIds(bull), null), config.constraintsList(j).hard)
            case FRONT_TEAT => FrontTeat.computePenalty(graph.cows.getOrElse(parsedCowIds(cow), null),
              graph.bulls.getOrElse(bullIds(bull), null), config.constraintsList(j).hard)
            case LIMBS => Limbs.computePenalty(graph.cows.getOrElse(parsedCowIds(cow), null),
              graph.bulls.getOrElse(bullIds(bull), null), config.constraintsList(j).hard)
            case MAMMARY_GLAND => MammaryGland.computePenalty(graph.cows.getOrElse(parsedCowIds(cow), null),
              graph.bulls.getOrElse(bullIds(bull), null), config.constraintsList(j).hard)
            case MEDIAN_SUSPENSION => MedianSuspension.computePenalty(graph.cows.getOrElse(parsedCowIds(cow), null),
              graph.bulls.getOrElse(bullIds(bull), null), config.constraintsList(j).hard)
            case MILK_IMPROVEMENT => graph.bulls.getOrElse(bullIds(bull), null).milk
            case PROTEIN_QUALITY => Protein.computePenalty(graph.cows.getOrElse(parsedCowIds(cow), null),
              graph.bulls.getOrElse(bullIds(bull), null), config.constraintsList(j).hard)
            case PROTEIN_IMPROVEMENT => Protein.computeImprovement(graph.bulls.getOrElse(bullIds(bull), null))
            case SIDE_VIEW_LIMB => SideViewLimb.computePenalty(graph.cows.getOrElse(parsedCowIds(cow), null),
              graph.bulls.getOrElse(bullIds(bull), null), config.constraintsList(j).hard)
            case SOMATIC_CELLS => SomaticCells.computePenalty(graph.cows.getOrElse(parsedCowIds(cow), null),
              graph.bulls.getOrElse(bullIds(bull), null), config.constraintsList(j).hard)
            case TEAT_LENGTH => TeatLength.computePenalty(graph.cows.getOrElse(parsedCowIds(cow), null),
              graph.bulls.getOrElse(bullIds(bull), null), config.constraintsList(j).hard)
            case UDDER_DEPTH => UdderDepth.computePenalty(graph.cows.getOrElse(parsedCowIds(cow), null),
              graph.bulls.getOrElse(bullIds(bull), null), config.constraintsList(j).hard)
          }

          if (penalty == Bovine.SENTINEL_VALUE) bullForCows(cow).removeValue(bull)
          else costMatrix(j)(cow)(bull) = penalty
        }
      }
    }

    //Matrix is filled now we can calculate the value
    for (constraintNumber <- 0 until nbConstraint) {
      config.constraintsList(constraintNumber).ctrName match {
        case CONSANGUINITY_THRESHOLD | CONSANGUINITY_AVG | BACK_TEAT | BACK_VIEW_LIMB | CALVING | FAT_QUALITY
        | FOOT_ANGLE | FRONT_TEAT | LIMBS | MAMMARY_GLAND | MEDIAN_SUSPENSION | PROTEIN_QUALITY | SIDE_VIEW_LIMB
        | SOMATIC_CELLS | TEAT_LENGTH | UDDER_DEPTH => {
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
          }
          totalCost(constraintNumber) = sum(costs(constraintNumber))
        }
        case CONSANGUINITY_WORST => {
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
          }
          //we take the worst percentage for each cow
          totalCost(constraintNumber) = maximum(costs(constraintNumber))
        }
        /*case BACK_TEAT => {
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
          }
          totalCost(constraintNumber) = sum(costs(constraintNumber))
        }
        case BACK_VIEW_LIMB => {
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
          }
          totalCost(constraintNumber) = sum(costs(constraintNumber))
        }
        case CALVING => {
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
          }
          totalCost(constraintNumber) = sum(costs(constraintNumber))
        }
        case FAT_QUALITY => {
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
          }
          totalCost(constraintNumber) = sum(costs(constraintNumber))
        }*/
        case FAT_IMPROVEMENT => {
          totalCost(constraintNumber) = CPIntVar(0)
          Fat.IMPROVEMENT_FACTOR = (config.constraintsList(constraintNumber).threshold * Fat.FAT_MULTIPLIER).toInt
          val oldValues = Array.ofDim[Int](nbBulls)
          for(bull <- 0 until nbBulls) oldValues(bull) = graph.bulls.get(bullIds(bull)).get.milk
          val milkCost = Array.ofDim[CPIntVar](nbCows)
          for (i <- 0 until nbCows) milkCost(i) = CPIntVar(0 until 1000000)
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
            add(milkCost(i) === oldValues(bullForCows(i)))
          }
          constrainedVariables = constrainedVariables ++ milkCost
          add(sum(costs(constraintNumber)) >= (sum(milkCost)*Fat.IMPROVEMENT_FACTOR))
        }
        /*case FOOT_ANGLE => {
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
          }
          totalCost(constraintNumber) = sum(costs(constraintNumber))
        }
        case FRONT_TEAT => {
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
          }
          totalCost(constraintNumber) = sum(costs(constraintNumber))
        }
        case LIMBS => {
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
          }
          totalCost(constraintNumber) = sum(costs(constraintNumber))
        }
        case MAMMARY_GLAND => {
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
          }
          totalCost(constraintNumber) = sum(costs(constraintNumber))
        }
        case MEDIAN_SUSPENSION => {
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
          }
          totalCost(constraintNumber) = sum(costs(constraintNumber))
        }*/
        case MILK_IMPROVEMENT => {
          //set 0 to totalCost for this variable in order to absord the weight for the final sum
          totalCost(constraintNumber) = CPIntVar(0)
          Milk.THRESHOLD = config.constraintsList(constraintNumber).threshold.toInt
          Milk.COMPUTED_THRESHOLD = Milk.THRESHOLD * nbCows
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
          }
          add(sum(costs(constraintNumber)) >= Milk.COMPUTED_THRESHOLD)
        }
        /*case PROTEIN_QUALITY => {
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
          }
          totalCost(constraintNumber) = sum(costs(constraintNumber))
        }*/
        case PROTEIN_IMPROVEMENT => {
          totalCost(constraintNumber) = CPIntVar(0)
          Protein.IMPROVEMENT_FACTOR = (config.constraintsList(constraintNumber).threshold * Protein.PROTEIN_MULTIPLIER).toInt
          val oldValues = Array.ofDim[Int](nbBulls)
          for(bull <- 0 until nbBulls) oldValues(bull) = graph.bulls.get(bullIds(bull)).get.milk
          val milkCost = Array.ofDim[CPIntVar](nbCows)
          for (i <- 0 until nbCows) milkCost(i) = CPIntVar(0 until 1000000)
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
            add(milkCost(i) === oldValues(bullForCows(i)))
          }
          constrainedVariables = constrainedVariables ++ milkCost
          add(sum(costs(constraintNumber)) >= (sum(milkCost)*Protein.IMPROVEMENT_FACTOR))
        }
        /*case SIDE_VIEW_LIMB => {
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
          }
          totalCost(constraintNumber) = sum(costs(constraintNumber))
        }
        case SOMATIC_CELLS => {
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
          }
          totalCost(constraintNumber) = sum(costs(constraintNumber))
        }
        case TEAT_LENGTH => {
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
          }
          totalCost(constraintNumber) = sum(costs(constraintNumber))
        }
        case UDDER_DEPTH => {
          for(i <- 0 until nbCows) {
            val values = CPIntVar(costMatrix(constraintNumber)(i))
            costs(constraintNumber)(i) = values
            add(costMatrix(constraintNumber)(i)(bullForCows(i)) === costs(constraintNumber)(i))
          }
          totalCost(constraintNumber) = sum(costs(constraintNumber))
        }*/
      }
    }

    //create the array for all the constraints weight
    var weightArray = ListBuffer[Int]()
    for(c <- config.constraintsList) weightArray += c.weight
    //make a weighted sum of the constraint by their weight
    var wSum = weightedSum(weightArray.toArray, totalCost)
    //minimize the weighted sum
    minimize(wSum)

    //add all the constraints to the sequence
    constrainedVariables = constrainedVariables ++ bullForCows
    constrainedVariables = constrainedVariables ++ totalCost
    constrainedVariables = constrainedVariables :+ wSum
    for(i <- 0 until costs.length) constrainedVariables = constrainedVariables ++ costs(i)

    //assert that number of bulls is less or equal than the desirated value
    add(new AtMostNValue(bullForCows, nbMaxBulls))

    search {
      binaryFirstFail(constrainedVariables)
    }

    onSolution {
      println("Cost : "+(wSum.value/nbCows))
      //for(bull <- bullForCows) println(bull.value+" ")
      /*println("Calving difficulty : "+totalCost(1))
      println("Somatic cells : "+totalCost(2))
      println()*/
    }

    val stat  = start(nSols=1)
    println(stat)

  }
}
