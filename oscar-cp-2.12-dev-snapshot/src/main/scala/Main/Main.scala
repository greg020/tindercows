package Main

import tools._

import net.liftweb.json._
import net.liftweb.json.DefaultFormats

import scala.io.Source

object Main extends App {

  implicit val formats = DefaultFormats

  val text = Source.fromFile("data/config.json").mkString

  val json = parse(text)
  val config = json.extract[ConfigurationJSON]

  val pgmConfig = Configuration(config)
  //Cows.startModel(pgmConfig)

}
