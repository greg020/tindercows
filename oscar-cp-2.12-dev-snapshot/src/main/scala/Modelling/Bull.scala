package Modelling

import tools.CalvingDifficulty._
import tools.WebParsingResult

class Bull(nName: String) extends Bovine{
  name = nName
  var calvingQuality : CalvingDifficulty = DIFFICULT
  var fatImprovement : Double = 0
  var proteinImprovement : Double = 0

  override def toString: String = {
    var string = "Bull Name: "+name+", ids: "+ idA +" "+idB
      if (mother != null) string += "; Mother: "+mother.toString
      for (father <- fathers) {
        if (father != null) string += "; Father: "+ father.toString
      }
      string += " "+calvingQuality
      string += " "+birthdate
      string += " "+somaticCells
    return string
  }

  override def equals(obj: scala.Any): Boolean = {
    if (obj.isInstanceOf[Bull]) {
      val objAsCow = obj.asInstanceOf[Bull]
      return objAsCow.name.equals(this.name)
    }
    return false
  }
}

object Bull {

  def apply(result : WebParsingResult): Bull = {
    val bov = new Bull(if (result.shortName == "") result.longName else result.shortName)
    bov.idA = result.id._1
    bov.idB = result.id._2
    bov.birthdate = result.birthdate
    bov.name = bov.name.toLowerCase
    //bull.name = name
    bov.somaticCells = result.somaticCells.round.toInt
    bov.protein = result.protein
    bov.fat = result.fat
    bov.milk = result.milk
    bov.mammaryGland = result.mammalGland
    bov.udderDepth = result.udder
    bov.medianSuspension = result.suspension
    bov.frontTeat = result.frontTeat
    bov.backTeat = result.backTeat
    bov.limbs = result.foot
    bov.footAngle = result.angle
    bov.sideViewLimb = result.sideLimb
    bov.backViewLimb = result.backLimb
    bov.proteinImprovement = result.proteinImprovement
    bov.fatImprovement = result.fatImprovement
    bov.calvingQuality = result.calving
    bov
  }
}
