package Modelling

object SomaticCells {

  val PENALTY_LOWER_BOND = 300000
  val PENALTY_RANGE = 50000
  val SOMATIC_MULTIPLIER = 1000

  def computeValue(avg: Int) : Int = {
    val residue = avg - PENALTY_LOWER_BOND

    if (residue >= 0) {
      val dividend = residue / PENALTY_RANGE
      dividend
    } else 0
  }

  def computePenalty(mom: Cow, dad: Bull, isHard: Boolean) : Int = {
    val parentAvg = ((mom.somaticCells + dad.somaticCells) / 2) * SOMATIC_MULTIPLIER
    val momValue = mom.somaticCells * SOMATIC_MULTIPLIER

    (momValue, parentAvg) match {
      case x if x._1 <= 300000 && x._2 <= 300000 => 0
      case x if x._1 <= 300000 && x._2 > 300000 && x._2 <= 400000 => if (isHard) Bovine.SENTINEL_VALUE
      else computeValue(parentAvg)
      case x if x._1 <= 300000 && x._2 > 400000 => if (isHard) Bovine.SENTINEL_VALUE
      else Math.min(4, computeValue(parentAvg))
      //
      case x if x._1 > 300000 && x._1 <= 400000 && x._2 <= 300000 => 0
      case x if x._1 > 300000 && x._1 <= 400000 && x._2 > 300000 && x._2 <= 400000 => computeValue(parentAvg)
      case x if x._1 > 300000 && x._1 <= 400000 && x._2 > 400000 => if (isHard) Bovine.SENTINEL_VALUE
      else Math.min(4, computeValue(parentAvg))
      //
      case x if x._1 > 400000 && x._2 <= 300000 => 0
      case x if x._1 > 400000 && x._2 > 300000 && x._2 <= 400000 => Math.min(4, computeValue(parentAvg))
      case x if x._1 > 400000 && x._2 > 400000 => Math.min(4, computeValue(parentAvg))
    }
  }

}
