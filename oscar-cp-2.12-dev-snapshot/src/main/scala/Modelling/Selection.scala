package Modelling

import java.text.SimpleDateFormat

import tools.SelectionJSON

trait Selection {

  def isAcceptable(cow: Cow) : Boolean

}

class DateSelection(from: String, to: String) extends Selection {

  val format = "dd/MM/yy"
  val regex = "[0-9]{2}\\/[0-9]{2}\\/[0-9]{2}"
  val parser = new SimpleDateFormat(format)

  val fromDate = parser.parse(from)
  val toDate = parser.parse(to)

  override def isAcceptable(cow: Cow) = {

    if (cow.birthdate == null || !cow.birthdate.matches(regex)) false
    val date = parser.parse(cow.birthdate)
    date.after(fromDate) && date.before(toDate)

  }

}

class IdSelection(from: (Int, Int), to: (Int,Int)) extends Selection {

  override def isAcceptable(cow: Cow) = {
    from._1 < cow.idA && from._2 < cow.idB && to._1 > cow.idA && to._2 > cow.idB
  }

}

class CalvingSelection(nbCalving: Int, sign: String) extends Selection {

  override def isAcceptable(cow: Cow) = {
    if (sign == ">") cow.nbCalving >= nbCalving
    else cow.nbCalving <= nbCalving
  }

}

object Selection {

  def apply(json: SelectionJSON): Selection = {
    json.name.toLowerCase match {
      case "id" => {
        val parseFrom = json.from.split(" ").map(x => x.toInt)
        val parseTo = json.to.split(" ").map(x => x.toInt)
        val idFrom : (Int, Int) = (parseFrom(0), parseFrom(1))
        val idTo : (Int, Int) = (parseTo(0), parseTo(1))
        new IdSelection(idFrom, idTo)
      }
      case "birth" => new DateSelection(json.from, json.to)
      case "calving" => new CalvingSelection(json.from.toInt, json.to)
    }
  }
}
