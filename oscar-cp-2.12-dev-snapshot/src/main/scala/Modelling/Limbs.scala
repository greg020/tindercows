package Modelling

object Limbs {

  def computePenalty(mom : Cow, dad : Bull, hardConstraint : Boolean = false) : Int = {

    (mom.limbs, dad.limbs) match {
      //Mom = -2
      case x if x._1 == -2 && x._2 <= -5 => if (hardConstraint) Bovine.SENTINEL_VALUE else 4
      case x if x._1 == -2 && x._2 > -5 && x._2 < 0 => if (hardConstraint) Bovine.SENTINEL_VALUE else 3
      case x if x._1 == -2 && x._2 == 0 => 2
      case x if x._1 == -2 && x._2 > 0 && x._2 < 5 => 1
      case x if x._1 == -2 && x._2 >= 5 => 0
      //Mom = -1
      case x if x._1 == -1 && x._2 <= -5 => if (hardConstraint) Bovine.SENTINEL_VALUE else 3
      case x if x._1 == -1 && x._2 > -5 && x._2 < 0 => 2
      case x if x._1 == -1 && x._2 == 0 => 1
      case x if x._1 == -1 && x._2 > 0 && x._2 < 5 => 0
      case x if x._1 == -1 && x._2 >= 5 => 0
      //Mom = 0
      case x if x._1 == 0 && x._2 <= -5 => 2
      case x if x._1 == 0 && x._2 > -5 && x._2 < 0 => 1
      case x if x._1 == 0 && x._2 == 0 => 0
      case x if x._1 == 0 && x._2 > 0 && x._2 < 5 => 0
      case x if x._1 == 0 && x._2 >= 5 => 0
      //Mom = 1
      case x if x._1 == 1 && x._2 <= -5 => 1
      case x if x._1 == 1 && x._2 > -5 && x._2 < 0 => 0
      case x if x._1 == 1 && x._2 == 0 => 0
      case x if x._1 == 1 && x._2 > 0 && x._2 < 5 => 0
      case x if x._1 == 1 && x._2 >= 5 => 0
      //Mom = 2
      case x if x._1 == 2 && x._2 <= -5 => 0
      case x if x._1 == 2 && x._2 > -5 && x._2 < 0 => 0
      case x if x._1 == 2 && x._2 == 0 => 0
      case x if x._1 == 2 && x._2 > 0 && x._2 < 5 => 0
      case x if x._1 == 2 && x._2 >= 5 => 0
    }
  }

}
