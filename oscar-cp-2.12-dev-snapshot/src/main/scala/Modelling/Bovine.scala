package Modelling

import tools.WebParsingResult

@SerialVersionUID(10L)
class Bovine extends Serializable {
  var idA: Int = 0
  var idB: Int = 0
  var name: String = ""
  var birthdate: String = ""
  var color: String = ""
  var somaticCells : Int = 0
  var mammaryGland : Int = 0
  var udderDepth : Int = 0
  var medianSuspension : Int = 0
  var frontTeat : Int = 0
  var backTeat : Int = 0
  var teatLength : Int = 0
  var limbs : Int = 0
  var footAngle : Int = 0
  var sideViewLimb : Int = 0
  var backViewLimb : Int = 0

  var protein : Double = 0
  var fat : Double = 0
  var milk : Int = 0

  var mother: Cow = null
  var fathers: Array[Bull] = new Array[Bull](2)
}

object Bovine {
  val SENTINEL_VALUE : Int = Int.MaxValue

  def apply(result : WebParsingResult): Bovine = {
    var bov : Bovine = new Bovine()
    bov.idA = result.id._1
    bov.idB = result.id._2
    bov.birthdate = result.birthdate
    bov.name = if (result.shortName == "") result.longName else result.shortName
    bov.name = bov.name.toLowerCase
    //bull.name = name
    bov.somaticCells = result.somaticCells.round.toInt
    bov.protein = result.protein
    bov.fat = result.fat
    bov.milk = result.milk
    bov.mammaryGland = result.mammalGland
    bov.udderDepth = result.udder
    bov.medianSuspension = result.suspension
    bov.frontTeat = result.frontTeat
    bov.backTeat = result.backTeat
    bov.limbs = result.foot
    bov.footAngle = result.angle
    bov.sideViewLimb = result.sideLimb
    bov.backViewLimb = result.backLimb
    bov
  }
}
