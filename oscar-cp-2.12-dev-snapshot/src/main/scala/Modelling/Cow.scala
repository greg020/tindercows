package Modelling

import tools.WebParsingResult

class Cow(nIdA: Integer, nIdB: Integer, nName: String) extends Bovine {
  idA = nIdA
  idB = nIdB
  name = nName
  // remembers the number of times the cow gave birth inside the farm
  var nbCalving: Int = 0
  // remembers the number of difficult birth (need of assistance, twins, born dead, ...)
  var nbBadCalving: Int = 0

  var fatTotal : Double = 0
  var fatNbMeasurements = 0

  var proteinTotal : Double = 0
  var proteinNbMeasurements = 0

  // Compute the calving difficulty : 1 if no calvings, the correct number otherwise
  def difficultyOfCalving() : Int = {
    if (nbCalving == 0) 1 else nbBadCalving
  }

  def computeProteinMean() : Double = {
    if (proteinNbMeasurements == 0) 0
    protein = proteinTotal / proteinNbMeasurements
    protein
  }

  def computeFatMean() : Double = {
    if (fatNbMeasurements == 0) 0
    fat = fatTotal / fatNbMeasurements
    fat
  }

  override def toString: String = {
    return "Cow Name: "+name+", id: "+idA+" "+idB+", nb calvings: "+nbCalving+", nb bad calvings: "+nbBadCalving+" Mother : "+mother
  }

  override def equals(obj: scala.Any): Boolean = {
    if (obj.isInstanceOf[Cow]) {
      val objAsCow = obj.asInstanceOf[Cow]
      return objAsCow.idA.equals(this.idA) && objAsCow.idB.equals(this.idB)
    }
    return false
  }
}

object Cow {

  def apply(result : WebParsingResult): Cow = {
    var bov = new Cow(result.id._1, result.id._2, if (result.shortName == "") result.longName else result.shortName)
    bov.birthdate = result.birthdate
    bov.name = bov.name.toLowerCase
    //bull.name = name
    bov.somaticCells = result.somaticCells.round.toInt
    bov.protein = result.protein
    bov.fat = result.fat
    bov.milk = result.milk
    bov.mammaryGland = result.mammalGland
    bov.udderDepth = result.udder
    bov.medianSuspension = result.suspension
    bov.frontTeat = result.frontTeat
    bov.backTeat = result.backTeat
    bov.limbs = result.foot
    bov.footAngle = result.angle
    bov.sideViewLimb = result.sideLimb
    bov.backViewLimb = result.backLimb
    bov.nbCalving = 0
    bov.nbBadCalving = 0
    bov
  }

  var MEAN_FAT : Double = 0
  var MEAN_PROTEIN : Double = 0
}
