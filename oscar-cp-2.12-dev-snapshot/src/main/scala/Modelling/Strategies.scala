package Modelling

/**
  * Provide a Trait for implementing counting strategies
  */
trait Strategy {

  /**
    * Add the value to the intern state of the strategy
    * @param value The value added to the strategy
    * @return 0 if everything went fine and -1 if the value violates the strategy
    */
  def add(value: Int) : Int

  /**
    * Gives the result of the counting strategy
    * @return the result of the counting strategy
    */
  def result() : Int

  /**
    * Restore the original state of the strategy
    */
  def reset() : Unit

}

/**
  * Strategy computing the average of all the added positive or zero values
  */
class AvgStrategy() extends Strategy {

  private var functionalValue = 0
  private var cpt : Int = 0

  /**
    * Add the value to the internal state of the strategy
    * @param value The value added to the strategy
    * @return 0 if the added value was positive and else -1
    */
  override def add(value: Int): Int = {
    if (value < 0) -1
    functionalValue += value
    cpt += 1
    0
  }

  /**
    * Compute and return the average of all the added values
    * @return the average of all the added values
    */
  override def result(): Int = {
    functionalValue / cpt
  }

  /**
    * Restore the internal state as if no values were added
    */
  override def reset(): Unit = {
    cpt = 0
    functionalValue = 0
  }
}

/**
  * Strategy accepting values lower or greater than a certain threshold
  * @param value the threshold
  * @param beLower true if the values must be lower, false if the values must be greater (stricly in both cases)
  */
class ThresholdStrategy(value: Int, beLower: Boolean) extends Strategy {

  private var trshld = value
  private var lower = beLower

  /**
    * Check if the value corresponds to the strategy
    * @param value The value being evaluated by the strategy
    * @return 0 if the value is valid by the strategy else -1
    */
  override def add(value: Int): Int = {
    if(lower && value > trshld) -1
    if(!lower && value < trshld) -1
    0
  }

  /**
    * Return the threshold of the strategy
    * @return the threshold
    */
  override def result(): Int = trshld

  /**
    * Inherited code, does nothing
    */
  override def reset(): Unit = {}
}

/**
  * Strategy computing the less or greater value being added
  * @param isMin true if you want to keep the lowest value, false for the greatest
  */
class MinOrMaxValue(isMin: Boolean) extends Strategy {

  private var min = isMin
  private var fctValue = if (isMin)  Int.MaxValue else Int.MinValue

  /**
    * Check if the value is lower/greater than the current value and update if needed
    * @param value The value added to the strategy
    * @return -1 if the value is negative, 0 otherwise
    */
  override def add(value: Int): Int = {
    if(value < 0) -1
    if(min && value < fctValue) fctValue = value
    if(!min && value > fctValue) fctValue = value
    0
  }

  /**
    * Return the lowest/greatest value computed
    * @return the result of the counting strategy
    */
  override def result(): Int = fctValue

  /**
    * Restore the state to the initial configuration
    */
  override def reset(): Unit = fctValue = if(min) Int.MaxValue else Int.MinValue
}
