package Modelling

import tools.CalvingDifficulty

object Calving {

  val CALVING_MULTIPLIER = 10

  /**
    * Get the calving difficulty score between a cow & a bull.
    * @param mom The mother of the veal.
    * @param dad One of the possible father for the veal.
    * @return A score corresponding to the calving difficulty.
    */
  def difficultyScore(mom : Cow, dad : Bull) : Int = {
    var result = 0
    //If we do not know the calving quality of the father or if it's difficult we compute
    //the calving difficulty from avg(#dCalving(mom), #dCalving(mom.mother))
    if (dad.calvingQuality == CalvingDifficulty.DIFFICULT) {
      //The multiplier is to assert that we work with Int instead of Double
      result = if (mom.mother != null)
        ((mom.difficultyOfCalving()+mom.mother.difficultyOfCalving())/2) * CALVING_MULTIPLIER
      else mom.difficultyOfCalving() * CALVING_MULTIPLIER
    }
    result
  }

}
