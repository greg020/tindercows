package Modelling

import scala.collection.mutable
import scala.io.Source
import tools.Duality

object Consanguinity {

  val CONSANGUINITY_MULTIPLIER = 1000
  val CONSANGUINITY_MAX_LVL = 5

  /**
    * Computes the consanguinity value between a bull & and a cow and their genealogy
    * @param mom Mother of the veal
    * @param potentialDad Father of the veal
    * @return Consanguinity value
    */
  def consanguinity(mom: Cow, potentialDad: Bull): Int = {
    var mapping: Map[Bovine,(Int, String)] = Map()
    var alreadyProcessed: Set[Bovine] = Set()
    val consanguinityTable = Array.ofDim[Int](5,5)

    val lines = Source.fromFile("data/consanguinity.txt").getLines.filter(!_.isEmpty).toArray
    for (i <- 0 until lines.length) {
      var line: Array[String] = lines(i).split(" ")
      for (j <- 0 until line.length) consanguinityTable(i)(j) = (line(j).toDouble * CONSANGUINITY_MULTIPLIER).toInt
    }
    //Consanguinity value
    var percentage: Int = 0
    //eventual dualities
    var dualities = new Duality
    var dualKey : String = null
    //initialize Queue to do a BFS
    var fifo = mutable.Queue[(Bovine,(Int,String))]()
    //Add parents to the queue with level 0
    fifo += mom -> (0,null)
    fifo += potentialDad -> (0,null)

    //BFS of the genealogy until a certain level
    while (!fifo.isEmpty) {
      dualKey = null //reset duality key
      var (elem, elemInfo) = fifo.dequeue()
      //Did we already computed consanguinity for this element ?
      if (!alreadyProcessed.contains(elem)) {
        //Did we already seen this element ?
        if (mapping.contains(elem)) {
          //add the correct consanguinity value to the percentage
          var csg = consanguinityTable(elemInfo._1)(mapping(elem)._1)
          //check if the bovine is from a multi-dad case
          if(elemInfo._2 != null) {
            //instead of adding the consanguinity we update the duality
            dualities.updateConflict(elemInfo._2, csg.toString)
          }else{
            //we simply add the consanguinity to the current percentage
            percentage += csg
          }
          //remove this element from the mapping
          mapping -= elem
          //and add it to the nodes already computed
          alreadyProcessed += elem
        } else {
          if (elem.fathers(1) != null) { //we have two dads
            val key = if (elemInfo._2 != null) elemInfo._2 else null //check if the bovine is from a multi dad case
            //and get the key or null if not
            dualKey = dualities.addConflict(key) //create new duality and get the key
          }
          mapping += (elem -> elemInfo)
          //if we are not at the max level minus 1 yet then add parents
          if (elemInfo._1 < CONSANGUINITY_MAX_LVL -1) {
            if(elem.mother != null) fifo += (elem.mother -> (elemInfo._1+1,null)) //add the mother
            if(elem.fathers(0) != null) fifo += (elem.fathers(0) -> (elemInfo._1+1,
              if (dualKey == null) null else dualKey+"_0")) //add the father
            if(elem.fathers(1) != null) fifo += (elem.fathers(1) -> (elemInfo._1+1,
              if (dualKey == null) null else dualKey+"_1")) //add the second father
          }
        }
      }
    }

    percentage += dualities.getValue() //update percentage with multi dad case
    return percentage
  }

}
