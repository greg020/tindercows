package Modelling

object Protein {

  val PROTEIN_MULTIPLIER = 100
  var IMPROVEMENT_FACTOR = 0

  def computePenalty(mom : Cow, dad : Bull, hardConstraint : Boolean = false) : Int = {

    (mom.protein, dad.proteinImprovement) match {
      //Mom < M
      case x if x._1 < Cow.MEAN_PROTEIN && x._2 < 0 => if (hardConstraint) Bovine.SENTINEL_VALUE else 4
      case x if x._1 < Cow.MEAN_PROTEIN && x._2 >= 0 => 0
      //Mom >= M
      case x if x._1 >= Cow.MEAN_PROTEIN && x._2 < 0 => 2
      case x if x._1 >= Cow.MEAN_PROTEIN && x._2 >= 0 => 0
    }
  }

  def computeImprovement(dad : Bull) : Int = {
    if (dad.proteinImprovement == 0) Bovine.SENTINEL_VALUE

    dad.milk * (dad.proteinImprovement * PROTEIN_MULTIPLIER).toInt
  }

}
