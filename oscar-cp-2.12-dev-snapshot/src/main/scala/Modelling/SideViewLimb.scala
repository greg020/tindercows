package Modelling

object SideViewLimb {

  def computePenalty(mom : Cow, dad : Bull, hardConstraint : Boolean = false) : Int = {

    (mom.sideViewLimb, dad.sideViewLimb) match {
      //Mom = -2
      case x if x._1 == -2 && x._2 <= -5 => 0
      case x if x._1 == -2 && x._2 > -5 && x._2 < 0 => 0
      case x if x._1 == -2 && x._2 == 0 => 0
      case x if x._1 == -2 && x._2 > 0 && x._2 < 5 => 0
      case x if x._1 == -2 && x._2 >= 5 => 0
      //Mom = -1
      case x if x._1 == -1 && x._2 <= -5 => 0
      case x if x._1 == -1 && x._2 > -5 && x._2 < 0 => 0
      case x if x._1 == -1 && x._2 == 0 => 0
      case x if x._1 == -1 && x._2 > 0 && x._2 < 5 => 0
      case x if x._1 == -1 && x._2 >= 5 => 1
      //Mom = 0
      case x if x._1 == 0 && x._2 <= -5 => 0
      case x if x._1 == 0 && x._2 > -5 && x._2 < 0 => 0
      case x if x._1 == 0 && x._2 == 0 => 0
      case x if x._1 == 0 && x._2 > 0 && x._2 < 5 => 1
      case x if x._1 == 0 && x._2 >= 5 => 2
      //Mom = 1
      case x if x._1 == 1 && x._2 <= -5 => 0
      case x if x._1 == 1 && x._2 > -5 && x._2 < 0 => 0
      case x if x._1 == 1 && x._2 == 0 => 1
      case x if x._1 == 1 && x._2 > 0 && x._2 < 5 => 2
      case x if x._1 == 1 && x._2 >= 5 => if (hardConstraint) Bovine.SENTINEL_VALUE else 3
      //Mom = 2
      case x if x._1 == 2 && x._2 <= -5 => 0
      case x if x._1 == 2 && x._2 > -5 && x._2 < 0 => 1
      case x if x._1 == 2 && x._2 == 0 => 2
      case x if x._1 == 2 && x._2 > 0 && x._2 < 5 => if (hardConstraint) Bovine.SENTINEL_VALUE else 3
      case x if x._1 == 2 && x._2 >= 5 => if (hardConstraint) Bovine.SENTINEL_VALUE else 4
    }
  }

}
