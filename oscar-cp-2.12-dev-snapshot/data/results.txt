Nb cows : 20, nb bulls : 15, nb max bulls : 2
objective tightened to 585 lb:584
Cost : 29
nNodes: 2
nFails: 2
time(ms): 16
completed: true
timeInTrail: 0
nSols: 1

Time : 447ms
Nb cows : 20, nb bulls : 15, nb max bulls : 2
objective tightened to 6640 lb:6639
Cost : 332
nNodes: 2
nFails: 2
time(ms): 20
completed: true
timeInTrail: 0
nSols: 1

Time : 462ms

Nb cows : 20, nb bulls : 15, nb max bulls : 2
objective tightened to 390 lb:389
Cost : 19
nNodes: 2
nFails: 2
time(ms): 18
completed: true
timeInTrail: 1
nSols: 1

Time : 439ms
Nb cows : 20, nb bulls : 15, nb max bulls : 2
objective tightened to 195 lb:194
Cost : 9
nNodes: 2
nFails: 2
time(ms): 53
completed: true
timeInTrail: 0
nSols: 1

Time : 864ms
Nb cows : 20, nb bulls : 15, nb max bulls : 2
objective tightened to 4881 lb:4880
Cost : 244
nNodes: 2
nFails: 2
time(ms): 19
completed: true
timeInTrail: 0
nSols: 1

Time : 434ms
********************************************
Nb cows : 20, nb bulls : 15, nb max bulls : 5
objective tightened to 6640 lb:6639
Cost : 332
nNodes: 42
nFails: 22
time(ms): 17
completed: true
timeInTrail: 1
nSols: 1

Time : 480ms
Nb cows : 20, nb bulls : 15, nb max bulls : 5
objective tightened to 2147 lb:2146
Cost : 107
nNodes: 40
nFails: 21
time(ms): 20
completed: true
timeInTrail: 0
nSols: 1

Time : 416ms
Nb cows : 20, nb bulls : 15, nb max bulls : 5
objective tightened to 3513 lb:3512
Cost : 175
nNodes: 40
nFails: 21
time(ms): 20
completed: true
timeInTrail: 0
nSols: 1

Time : 958ms
Nb cows : 20, nb bulls : 15, nb max bulls : 5
objective tightened to 390 lb:389
Cost : 19
nNodes: 40
nFails: 21
time(ms): 19
completed: true
timeInTrail: 0
nSols: 1

Time : 398ms
Nb cows : 20, nb bulls : 15, nb max bulls : 5
objective tightened to 2342 lb:2341
Cost : 117
nNodes: 40
nFails: 21
time(ms): 20
completed: true
timeInTrail: 2
nSols: 1

Time : 423ms
********************************************
Nb cows : 20, nb bulls : 15, nb max bulls : 10
objective tightened to 3318 lb:3317
Cost : 165
nNodes: 42
nFails: 22
time(ms): 19
completed: true
timeInTrail: 0
nSols: 1

Time : 480ms
Nb cows : 20, nb bulls : 15, nb max bulls : 10
objective tightened to 1952 lb:1951
Cost : 97
nNodes: 42
nFails: 22
time(ms): 16
completed: true
timeInTrail: 1
nSols: 1

Time : 436ms
Nb cows : 20, nb bulls : 15, nb max bulls : 10
objective tightened to 1170 lb:1169
Cost : 58
nNodes: 40
nFails: 21
time(ms): 18
completed: true
timeInTrail: 0
nSols: 1

Time : 921ms
Nb cows : 20, nb bulls : 15, nb max bulls : 10
objective tightened to 9765 lb:9764
Cost : 488
nNodes: 40
nFails: 21
time(ms): 23
completed: true
timeInTrail: 1
nSols: 1

Time : 468ms
Nb cows : 20, nb bulls : 15, nb max bulls : 10
objective tightened to 1757 lb:1756
Cost : 87
nNodes: 46
nFails: 24
time(ms): 21
completed: true
timeInTrail: 0
nSols: 1

Time : 502ms
********************************************
Nb cows : 50, nb bulls : 15, nb max bulls : 2
objective tightened to 5269 lb:5268
Cost : 105
nNodes: 4
nFails: 3
time(ms): 14
completed: true
timeInTrail: 1
nSols: 1

Time : 558ms
Nb cows : 50, nb bulls : 15, nb max bulls : 2
objective tightened to 9371 lb:9370
Cost : 187
nNodes: 4
nFails: 3
time(ms): 14
completed: true
timeInTrail: 0
nSols: 1

Time : 556ms
Nb cows : 50, nb bulls : 15, nb max bulls : 2
objective tightened to 8201 lb:8200
Cost : 164
nNodes: 2
nFails: 2
time(ms): 45
completed: true
timeInTrail: 0
nSols: 1

Time : 956ms
Nb cows : 50, nb bulls : 15, nb max bulls : 2
objective tightened to 6051 lb:6050
Cost : 121
nNodes: 4
nFails: 3
time(ms): 15
completed: true
timeInTrail: 0
nSols: 1

Time : 584ms
Nb cows : 50, nb bulls : 15, nb max bulls : 2
objective tightened to 23041 lb:23040
Cost : 460
nNodes: 4
nFails: 3
time(ms): 15
completed: true
timeInTrail: 0
nSols: 1

Time : 569ms
********************************************
Nb cows : 50, nb bulls : 15, nb max bulls : 5
objective tightened to 3318 lb:3317
Cost : 66
nNodes: 104
nFails: 53
time(ms): 22
completed: true
timeInTrail: 0
nSols: 1

Time : 559ms
Nb cows : 50, nb bulls : 15, nb max bulls : 5
objective tightened to 9762 lb:9761
Cost : 195
nNodes: 102
nFails: 52
time(ms): 29
completed: true
timeInTrail: 0
nSols: 1

Time : 599ms
Nb cows : 50, nb bulls : 15, nb max bulls : 5
objective tightened to 8202 lb:8201
Cost : 164
nNodes: 100
nFails: 51
time(ms): 36
completed: true
timeInTrail: 0
nSols: 1

Time : 1089ms
Nb cows : 50, nb bulls : 15, nb max bulls : 5
objective tightened to 2536 lb:2535
Cost : 50
nNodes: 102
nFails: 52
time(ms): 23
completed: true
timeInTrail: 1
nSols: 1

Time : 567ms
Nb cows : 50, nb bulls : 15, nb max bulls : 5
objective tightened to 10151 lb:10150
Cost : 203
nNodes: 104
nFails: 53
time(ms): 21
completed: true
timeInTrail: 0
nSols: 1

Time : 607ms
********************************************
Nb cows : 50, nb bulls : 15, nb max bulls : 10
objective tightened to 7028 lb:7027
Cost : 140
nNodes: 100
nFails: 51
time(ms): 21
completed: true
timeInTrail: 0
nSols: 1

Time : 548ms
Nb cows : 50, nb bulls : 15, nb max bulls : 10
objective tightened to 6834 lb:6833
Cost : 136
nNodes: 104
nFails: 53
time(ms): 21
completed: true
timeInTrail: 0
nSols: 1

Time : 551ms
Nb cows : 50, nb bulls : 15, nb max bulls : 10
objective tightened to 12105 lb:12104
Cost : 242
nNodes: 114
nFails: 58
time(ms): 27
completed: true
timeInTrail: 0
nSols: 1

Time : 1208ms
Nb cows : 50, nb bulls : 15, nb max bulls : 10
objective tightened to 10739 lb:10738
Cost : 214
nNodes: 102
nFails: 52
time(ms): 31
completed: true
timeInTrail: 0
nSols: 1

Time : 575ms
Nb cows : 50, nb bulls : 15, nb max bulls : 10
objective tightened to 3708 lb:3707
Cost : 74
nNodes: 104
nFails: 53
time(ms): 21
completed: true
timeInTrail: 0
nSols: 1

Time : 583ms
********************************************
Nb cows : 100, nb bulls : 15, nb max bulls : 2
objective tightened to 10735 lb:10734
Cost : 107
nNodes: 6
nFails: 4
time(ms): 17
completed: true
timeInTrail: 1
nSols: 1

Time : 675ms
Nb cows : 100, nb bulls : 15, nb max bulls : 2
objective tightened to 13080 lb:13079
Cost : 130
nNodes: 4
nFails: 3
time(ms): 15
completed: true
timeInTrail: 0
nSols: 1

Time : 671ms
Nb cows : 100, nb bulls : 15, nb max bulls : 2
objective tightened to 24994 lb:24993
Cost : 249
nNodes: 16
nFails: 9
time(ms): 23
completed: true
timeInTrail: 0
nSols: 1

Time : 1371ms
Nb cows : 100, nb bulls : 15, nb max bulls : 2
objective tightened to 9951 lb:9950
Cost : 99
nNodes: 6
nFails: 4
time(ms): 21
completed: true
timeInTrail: 0
nSols: 1

Time : 708ms
Nb cows : 100, nb bulls : 15, nb max bulls : 2
objective tightened to 14447 lb:14446
Cost : 144
nNodes: 6
nFails: 4
time(ms): 19
completed: true
timeInTrail: 0
nSols: 1

Time : 696ms
********************************************
Nb cows : 100, nb bulls : 15, nb max bulls : 5
objective tightened to 16396 lb:16395
Cost : 163
nNodes: 204
nFails: 103
time(ms): 32
completed: true
timeInTrail: 0
nSols: 1

Time : 733ms
Nb cows : 100, nb bulls : 15, nb max bulls : 5
objective tightened to 27140 lb:27139
Cost : 271
nNodes: 200
nFails: 101
time(ms): 33
completed: true
timeInTrail: 0
nSols: 1

Time : 681ms
Nb cows : 100, nb bulls : 15, nb max bulls : 5
objective tightened to 13081 lb:13080
Cost : 130
nNodes: 200
nFails: 101
time(ms): 65
completed: true
timeInTrail: 0
nSols: 1

Time : 1302ms
Nb cows : 100, nb bulls : 15, nb max bulls : 5
objective tightened to 12885 lb:12884
Cost : 128
nNodes: 202
nFails: 102
time(ms): 31
completed: true
timeInTrail: 2
nSols: 1

Time : 700ms
Nb cows : 100, nb bulls : 15, nb max bulls : 5
objective tightened to 16399 lb:16398
Cost : 163
nNodes: 208
nFails: 105
time(ms): 30
completed: true
timeInTrail: 0
nSols: 1

Time : 757ms
********************************************
Nb cows : 100, nb bulls : 15, nb max bulls : 10
objective tightened to 12297 lb:12296
Cost : 122
nNodes: 204
nFails: 103
time(ms): 34
completed: true
timeInTrail: 0
nSols: 1

Time : 745ms
Nb cows : 100, nb bulls : 15, nb max bulls : 10
objective tightened to 21865 lb:21864
Cost : 218
nNodes: 204
nFails: 103
time(ms): 38
completed: true
timeInTrail: 1
nSols: 1

Time : 692ms
Nb cows : 100, nb bulls : 15, nb max bulls : 10
objective tightened to 11910 lb:11909
Cost : 119
nNodes: 206
nFails: 104
time(ms): 72
completed: true
timeInTrail: 1
nSols: 1

Time : 1448ms
Nb cows : 100, nb bulls : 15, nb max bulls : 10
objective tightened to 14448 lb:14447
Cost : 144
nNodes: 208
nFails: 105
time(ms): 36
completed: true
timeInTrail: 1
nSols: 1

Time : 703ms
Nb cows : 100, nb bulls : 15, nb max bulls : 10
objective tightened to 9564 lb:9563
Cost : 95
nNodes: 206
nFails: 104
time(ms): 38
completed: true
timeInTrail: 1
nSols: 1

Time : 758ms
********************************************
Nb cows : 20, nb bulls : 20, nb max bulls : 2
objective tightened to 3514 lb:3513
Cost : 175
nNodes: 2
nFails: 2
time(ms): 17
completed: true
timeInTrail: 0
nSols: 1

Time : 507ms
Nb cows : 20, nb bulls : 20, nb max bulls : 2
objective tightened to 4686 lb:4685
Cost : 234
nNodes: 2
nFails: 2
time(ms): 19
completed: true
timeInTrail: 0
nSols: 1

Time : 434ms
Nb cows : 20, nb bulls : 20, nb max bulls : 2
objective tightened to 7030 lb:7029
Cost : 351
nNodes: 2
nFails: 2
time(ms): 20
completed: true
timeInTrail: 0
nSols: 1

Time : 815ms
Nb cows : 20, nb bulls : 20, nb max bulls : 2
objective tightened to 1171 lb:1170
Cost : 58
nNodes: 4
nFails: 3
time(ms): 17
completed: true
timeInTrail: 0
nSols: 1

Time : 473ms
Nb cows : 20, nb bulls : 20, nb max bulls : 2
objective tightened to 5271 lb:5270
Cost : 263
nNodes: 6
nFails: 4
time(ms): 18
completed: true
timeInTrail: 0
nSols: 1

Time : 493ms
********************************************
Nb cows : 20, nb bulls : 20, nb max bulls : 5
objective tightened to 2538 lb:2537
Cost : 126
nNodes: 44
nFails: 23
time(ms): 18
completed: true
timeInTrail: 0
nSols: 1

Time : 507ms
Nb cows : 20, nb bulls : 20, nb max bulls : 5
objective tightened to 2537 lb:2536
Cost : 126
nNodes: 40
nFails: 21
time(ms): 18
completed: true
timeInTrail: 0
nSols: 1

Time : 435ms
Nb cows : 20, nb bulls : 20, nb max bulls : 5
objective tightened to 781 lb:780
Cost : 39
nNodes: 40
nFails: 21
time(ms): 33
completed: true
timeInTrail: 0
nSols: 1

Time : 911ms
Nb cows : 20, nb bulls : 20, nb max bulls : 5
objective tightened to 0 lb:-1
Cost : 0
nNodes: 40
nFails: 21
time(ms): 21
completed: true
timeInTrail: 0
nSols: 1

Time : 458ms
Nb cows : 20, nb bulls : 20, nb max bulls : 5
objective tightened to 6833 lb:6832
Cost : 341
nNodes: 42
nFails: 22
time(ms): 20
completed: true
timeInTrail: 0
nSols: 1

Time : 500ms
********************************************
Nb cows : 20, nb bulls : 20, nb max bulls : 10
objective tightened to 5858 lb:5857
Cost : 292
nNodes: 44
nFails: 23
time(ms): 18
completed: true
timeInTrail: 0
nSols: 1

Time : 500ms
Nb cows : 20, nb bulls : 20, nb max bulls : 10
objective tightened to 1756 lb:1755
Cost : 87
nNodes: 42
nFails: 22
time(ms): 17
completed: true
timeInTrail: 0
nSols: 1

Time : 482ms
Nb cows : 20, nb bulls : 20, nb max bulls : 10
objective tightened to 2147 lb:2146
Cost : 107
nNodes: 42
nFails: 22
time(ms): 33
completed: true
timeInTrail: 1
nSols: 1

Time : 943ms
Nb cows : 20, nb bulls : 20, nb max bulls : 10
objective tightened to 1366 lb:1365
Cost : 68
nNodes: 40
nFails: 21
time(ms): 22
completed: true
timeInTrail: 0
nSols: 1

Time : 482ms
Nb cows : 20, nb bulls : 20, nb max bulls : 10
objective tightened to 780 lb:779
Cost : 39
nNodes: 44
nFails: 23
time(ms): 18
completed: true
timeInTrail: 0
nSols: 1

Time : 482ms
********************************************
Nb cows : 50, nb bulls : 20, nb max bulls : 2
objective tightened to 8393 lb:8392
Cost : 167
nNodes: 2
nFails: 2
time(ms): 16
completed: true
timeInTrail: 0
nSols: 1

Time : 592ms
Nb cows : 50, nb bulls : 20, nb max bulls : 2
objective tightened to 12103 lb:12102
Cost : 242
nNodes: 2
nFails: 2
time(ms): 21
completed: true
timeInTrail: 0
nSols: 1

Time : 623ms
Nb cows : 50, nb bulls : 20, nb max bulls : 2
objective tightened to 5270 lb:5269
Cost : 105
nNodes: 4
nFails: 3
time(ms): 19
completed: true
timeInTrail: 0
nSols: 1

Time : 1132ms
Nb cows : 50, nb bulls : 20, nb max bulls : 2
objective tightened to 12691 lb:12690
Cost : 253
nNodes: 4
nFails: 3
time(ms): 14
completed: true
timeInTrail: 0
nSols: 1

Time : 613ms
Nb cows : 50, nb bulls : 20, nb max bulls : 2
objective tightened to 5075 lb:5074
Cost : 101
nNodes: 2
nFails: 2
time(ms): 20
completed: true
timeInTrail: 0
nSols: 1

Time : 603ms
********************************************
Nb cows : 50, nb bulls : 20, nb max bulls : 5
objective tightened to 6052 lb:6051
Cost : 121
nNodes: 100
nFails: 51
time(ms): 22
completed: true
timeInTrail: 0
nSols: 1

Time : 589ms
Nb cows : 50, nb bulls : 20, nb max bulls : 5
objective tightened to 4099 lb:4098
Cost : 81
nNodes: 100
nFails: 51
time(ms): 30
completed: true
timeInTrail: 0
nSols: 1

Time : 587ms
Nb cows : 50, nb bulls : 20, nb max bulls : 5
objective tightened to 4881 lb:4880
Cost : 97
nNodes: 102
nFails: 52
time(ms): 26
completed: true
timeInTrail: 0
nSols: 1

Time : 1191ms
Nb cows : 50, nb bulls : 20, nb max bulls : 5
objective tightened to 8979 lb:8978
Cost : 179
nNodes: 102
nFails: 52
time(ms): 21
completed: true
timeInTrail: 0
nSols: 1

Time : 653ms
Nb cows : 50, nb bulls : 20, nb max bulls : 5
objective tightened to 780 lb:779
Cost : 15
nNodes: 100
nFails: 51
time(ms): 24
completed: true
timeInTrail: 0
nSols: 1

Time : 567ms
********************************************
Nb cows : 50, nb bulls : 20, nb max bulls : 10
objective tightened to 3513 lb:3512
Cost : 70
nNodes: 100
nFails: 51
time(ms): 24
completed: true
timeInTrail: 0
nSols: 1

Time : 538ms
Nb cows : 50, nb bulls : 20, nb max bulls : 10
objective tightened to 8394 lb:8393
Cost : 167
nNodes: 104
nFails: 53
time(ms): 18
completed: true
timeInTrail: 0
nSols: 1

Time : 589ms
Nb cows : 50, nb bulls : 20, nb max bulls : 10
objective tightened to 19920 lb:19919
Cost : 398
nNodes: 104
nFails: 53
time(ms): 27
completed: true
timeInTrail: 0
nSols: 1

Time : 1322ms
Nb cows : 50, nb bulls : 20, nb max bulls : 10
objective tightened to 6052 lb:6051
Cost : 121
nNodes: 104
nFails: 53
time(ms): 19
completed: true
timeInTrail: 0
nSols: 1

Time : 644ms
Nb cows : 50, nb bulls : 20, nb max bulls : 10
objective tightened to 9369 lb:9368
Cost : 187
nNodes: 104
nFails: 53
time(ms): 18
completed: true
timeInTrail: 0
nSols: 1

Time : 631ms
********************************************
Nb cows : 100, nb bulls : 20, nb max bulls : 2
objective tightened to 17961 lb:17960
Cost : 179
nNodes: 14
nFails: 8
time(ms): 22
completed: true
timeInTrail: 0
nSols: 1

Time : 796ms
Nb cows : 100, nb bulls : 20, nb max bulls : 2
objective tightened to 11322 lb:11321
Cost : 113
nNodes: 6
nFails: 4
time(ms): 18
completed: true
timeInTrail: 1
nSols: 1

Time : 721ms
Nb cows : 100, nb bulls : 20, nb max bulls : 2
objective tightened to 15617 lb:15616
Cost : 156
nNodes: 8
nFails: 5
time(ms): 19
completed: true
timeInTrail: 2
nSols: 1

Time : 1069ms
Nb cows : 100, nb bulls : 20, nb max bulls : 2
objective tightened to 15030 lb:15029
Cost : 150
nNodes: 4
nFails: 3
time(ms): 17
completed: true
timeInTrail: 0
nSols: 1

Time : 787ms
Nb cows : 100, nb bulls : 20, nb max bulls : 2
objective tightened to 24214 lb:24213
Cost : 242
nNodes: 8
nFails: 5
time(ms): 20
completed: true
timeInTrail: 0
nSols: 1

Time : 754ms
********************************************
Nb cows : 100, nb bulls : 20, nb max bulls : 5
objective tightened to 8782 lb:8781
Cost : 87
nNodes: 210
nFails: 106
time(ms): 30
completed: true
timeInTrail: 0
nSols: 1

Time : 805ms
Nb cows : 100, nb bulls : 20, nb max bulls : 5
objective tightened to 27335 lb:27334
Cost : 273
nNodes: 202
nFails: 102
time(ms): 28
completed: true
timeInTrail: 0
nSols: 1

Time : 791ms
Nb cows : 100, nb bulls : 20, nb max bulls : 5
objective tightened to 9173 lb:9172
Cost : 91
nNodes: 212
nFails: 107
time(ms): 82
completed: true
timeInTrail: 2
nSols: 1

Time : 1422ms
Nb cows : 100, nb bulls : 20, nb max bulls : 5
objective tightened to 18746 lb:18745
Cost : 187
nNodes: 202
nFails: 102
time(ms): 30
completed: true
timeInTrail: 0
nSols: 1

Time : 757ms
Nb cows : 100, nb bulls : 20, nb max bulls : 5
objective tightened to 5269 lb:5268
Cost : 52
nNodes: 208
nFails: 105
time(ms): 32
completed: true
timeInTrail: 0
nSols: 1

Time : 843ms
********************************************
Nb cows : 100, nb bulls : 20, nb max bulls : 10
objective tightened to 20695 lb:20694
Cost : 206
nNodes: 212
nFails: 107
time(ms): 30
completed: true
timeInTrail: 0
nSols: 1

Time : 798ms
Nb cows : 100, nb bulls : 20, nb max bulls : 10
objective tightened to 16202 lb:16201
Cost : 162
nNodes: 204
nFails: 103
time(ms): 32
completed: true
timeInTrail: 1
nSols: 1

Time : 881ms
Nb cows : 100, nb bulls : 20, nb max bulls : 10
objective tightened to 20498 lb:20497
Cost : 204
nNodes: 204
nFails: 103
time(ms): 40
completed: true
timeInTrail: 0
nSols: 1

Time : 1457ms
Nb cows : 100, nb bulls : 20, nb max bulls : 10
objective tightened to 10541 lb:10540
Cost : 105
nNodes: 212
nFails: 107
time(ms): 34
completed: true
timeInTrail: 1
nSols: 1

Time : 854ms
Nb cows : 100, nb bulls : 20, nb max bulls : 10
objective tightened to 20697 lb:20696
Cost : 206
nNodes: 206
nFails: 104
time(ms): 29
completed: true
timeInTrail: 1
nSols: 1

Time : 738ms
********************************************
Nb cows : 20, nb bulls : 40, nb max bulls : 2
objective tightened to 3904 lb:3903
Cost : 195
nNodes: 10
nFails: 6
time(ms): 18
completed: true
timeInTrail: 0
nSols: 1

Time : 637ms
Nb cows : 20, nb bulls : 40, nb max bulls : 2
objective tightened to 3514 lb:3513
Cost : 175
nNodes: 6
nFails: 4
time(ms): 19
completed: true
timeInTrail: 1
nSols: 1

Time : 568ms
Nb cows : 20, nb bulls : 40, nb max bulls : 2
objective tightened to 781 lb:780
Cost : 39
nNodes: 4
nFails: 3
time(ms): 37
completed: true
timeInTrail: 0
nSols: 1

Time : 1038ms
Nb cows : 20, nb bulls : 40, nb max bulls : 2
objective tightened to 7616 lb:7615
Cost : 380
nNodes: 2
nFails: 2
time(ms): 23
completed: true
timeInTrail: 0
nSols: 1

Time : 529ms
Nb cows : 20, nb bulls : 40, nb max bulls : 2
objective tightened to 976 lb:975
Cost : 48
nNodes: 2
nFails: 2
time(ms): 17
completed: true
timeInTrail: 0
nSols: 1

Time : 546ms
********************************************
Nb cows : 20, nb bulls : 40, nb max bulls : 5
objective tightened to 1561 lb:1560
Cost : 78
nNodes: 40
nFails: 21
time(ms): 32
completed: true
timeInTrail: 0
nSols: 1

Time : 580ms
Nb cows : 20, nb bulls : 40, nb max bulls : 5
objective tightened to 2928 lb:2927
Cost : 146
nNodes: 42
nFails: 22
time(ms): 18
completed: true
timeInTrail: 0
nSols: 1

Time : 564ms
Nb cows : 20, nb bulls : 40, nb max bulls : 5
objective tightened to 975 lb:974
Cost : 48
nNodes: 42
nFails: 22
time(ms): 28
completed: true
timeInTrail: 1
nSols: 1

Time : 1073ms
Nb cows : 20, nb bulls : 40, nb max bulls : 5
objective tightened to 5272 lb:5271
Cost : 263
nNodes: 40
nFails: 21
time(ms): 21
completed: true
timeInTrail: 0
nSols: 1

Time : 548ms
Nb cows : 20, nb bulls : 40, nb max bulls : 5
objective tightened to 585 lb:584
Cost : 29
nNodes: 40
nFails: 21
time(ms): 22
completed: true
timeInTrail: 0
nSols: 1

Time : 574ms
********************************************
Nb cows : 20, nb bulls : 40, nb max bulls : 10
objective tightened to 7030 lb:7029
Cost : 351
nNodes: 42
nFails: 22
time(ms): 22
completed: true
timeInTrail: 0
nSols: 1

Time : 559ms
Nb cows : 20, nb bulls : 40, nb max bulls : 10
objective tightened to 2928 lb:2927
Cost : 146
nNodes: 42
nFails: 22
time(ms): 20
completed: true
timeInTrail: 0
nSols: 1

Time : 588ms
Nb cows : 20, nb bulls : 40, nb max bulls : 10
objective tightened to 4100 lb:4099
Cost : 205
nNodes: 44
nFails: 23
time(ms): 20
completed: true
timeInTrail: 0
nSols: 1

Time : 976ms
Nb cows : 20, nb bulls : 40, nb max bulls : 10
objective tightened to 1952 lb:1951
Cost : 97
nNodes: 44
nFails: 23
time(ms): 18
completed: true
timeInTrail: 0
nSols: 1

Time : 593ms
Nb cows : 20, nb bulls : 40, nb max bulls : 10
objective tightened to 781 lb:780
Cost : 39
nNodes: 42
nFails: 22
time(ms): 19
completed: true
timeInTrail: 0
nSols: 1

Time : 558ms
********************************************
Nb cows : 50, nb bulls : 40, nb max bulls : 2
objective tightened to 12105 lb:12104
Cost : 242
nNodes: 6
nFails: 4
time(ms): 17
completed: true
timeInTrail: 0
nSols: 1

Time : 782ms
Nb cows : 50, nb bulls : 40, nb max bulls : 2
objective tightened to 11911 lb:11910
Cost : 238
nNodes: 4
nFails: 3
time(ms): 17
completed: true
timeInTrail: 0
nSols: 1

Time : 777ms
Nb cows : 50, nb bulls : 40, nb max bulls : 2
objective tightened to 4098 lb:4097
Cost : 81
nNodes: 8
nFails: 5
time(ms): 24
completed: true
timeInTrail: 0
nSols: 1

Time : 1455ms
Nb cows : 50, nb bulls : 40, nb max bulls : 2
objective tightened to 14057 lb:14056
Cost : 281
nNodes: 8
nFails: 5
time(ms): 21
completed: true
timeInTrail: 0
nSols: 1

Time : 752ms
Nb cows : 50, nb bulls : 40, nb max bulls : 2
objective tightened to 7416 lb:7415
Cost : 148
nNodes: 6
nFails: 4
time(ms): 16
completed: true
timeInTrail: 0
nSols: 1

Time : 732ms
********************************************
Nb cows : 50, nb bulls : 40, nb max bulls : 5
objective tightened to 5660 lb:5659
Cost : 113
nNodes: 102
nFails: 52
time(ms): 23
completed: true
timeInTrail: 0
nSols: 1

Time : 765ms
Nb cows : 50, nb bulls : 40, nb max bulls : 5
objective tightened to 10542 lb:10541
Cost : 210
nNodes: 108
nFails: 55
time(ms): 24
completed: true
timeInTrail: 0
nSols: 1

Time : 854ms
Nb cows : 50, nb bulls : 40, nb max bulls : 5
objective tightened to 18550 lb:18549
Cost : 371
nNodes: 110
nFails: 56
time(ms): 24
completed: true
timeInTrail: 0
nSols: 1

Time : 1295ms
Nb cows : 50, nb bulls : 40, nb max bulls : 5
objective tightened to 15815 lb:15814
Cost : 316
nNodes: 110
nFails: 56
time(ms): 21
completed: true
timeInTrail: 0
nSols: 1

Time : 780ms
Nb cows : 50, nb bulls : 40, nb max bulls : 5
objective tightened to 7418 lb:7417
Cost : 148
nNodes: 104
nFails: 53
time(ms): 24
completed: true
timeInTrail: 0
nSols: 1

Time : 776ms
********************************************
Nb cows : 50, nb bulls : 40, nb max bulls : 10
objective tightened to 8005 lb:8004
Cost : 160
nNodes: 112
nFails: 57
time(ms): 22
completed: true
timeInTrail: 0
nSols: 1

Time : 768ms
Nb cows : 50, nb bulls : 40, nb max bulls : 10
objective tightened to 11519 lb:11518
Cost : 230
nNodes: 104
nFails: 53
time(ms): 22
completed: true
timeInTrail: 0
nSols: 1

Time : 751ms
Nb cows : 50, nb bulls : 40, nb max bulls : 10
objective tightened to 3125 lb:3124
Cost : 62
nNodes: 104
nFails: 53
time(ms): 51
completed: true
timeInTrail: 0
nSols: 1

Time : 1357ms
Nb cows : 50, nb bulls : 40, nb max bulls : 10
objective tightened to 17963 lb:17962
Cost : 359
nNodes: 106
nFails: 54
time(ms): 23
completed: true
timeInTrail: 0
nSols: 1

Time : 802ms
Nb cows : 50, nb bulls : 40, nb max bulls : 10
objective tightened to 4880 lb:4879
Cost : 97
nNodes: 100
nFails: 51
time(ms): 27
completed: true
timeInTrail: 0
nSols: 1

Time : 701ms
********************************************
Nb cows : 100, nb bulls : 40, nb max bulls : 2
objective tightened to 18349 lb:18348
Cost : 183
nNodes: 18
nFails: 10
time(ms): 23
completed: true
timeInTrail: 0
nSols: 1

Time : 998ms
Nb cows : 100, nb bulls : 40, nb max bulls : 2
objective tightened to 13081 lb:13080
Cost : 130
nNodes: 14
nFails: 8
time(ms): 25
completed: true
timeInTrail: 0
nSols: 1

Time : 963ms
Nb cows : 100, nb bulls : 40, nb max bulls : 2
objective tightened to 29092 lb:29091
Cost : 290
nNodes: 12
nFails: 7
time(ms): 37
completed: true
timeInTrail: 1
nSols: 1

Time : 1718ms
Nb cows : 100, nb bulls : 40, nb max bulls : 2
objective tightened to 13274 lb:13273
Cost : 132
nNodes: 10
nFails: 6
time(ms): 24
completed: true
timeInTrail: 0
nSols: 1

Time : 911ms
Nb cows : 100, nb bulls : 40, nb max bulls : 2
objective tightened to 18742 lb:18741
Cost : 187
nNodes: 14
nFails: 8
time(ms): 24
completed: true
timeInTrail: 0
nSols: 1

Time : 1018ms
********************************************
Nb cows : 100, nb bulls : 40, nb max bulls : 5
objective tightened to 11125 lb:11124
Cost : 111
nNodes: 214
nFails: 108
time(ms): 30
completed: true
timeInTrail: 0
nSols: 1

Time : 978ms
Nb cows : 100, nb bulls : 40, nb max bulls : 5
objective tightened to 16789 lb:16788
Cost : 167
nNodes: 204
nFails: 103
time(ms): 28
completed: true
timeInTrail: 1
nSols: 1

Time : 935ms
Nb cows : 100, nb bulls : 40, nb max bulls : 5
objective tightened to 24211 lb:24210
Cost : 242
nNodes: 214
nFails: 108
time(ms): 40
completed: true
timeInTrail: 1
nSols: 1

Time : 1586ms
Nb cows : 100, nb bulls : 40, nb max bulls : 5
objective tightened to 20694 lb:20693
Cost : 206
nNodes: 222
nFails: 112
time(ms): 36
completed: true
timeInTrail: 1
nSols: 1

Time : 988ms
Nb cows : 100, nb bulls : 40, nb max bulls : 5
objective tightened to 20694 lb:20693
Cost : 206
nNodes: 204
nFails: 103
time(ms): 30
completed: true
timeInTrail: 0
nSols: 1

Time : 868ms
********************************************
Nb cows : 100, nb bulls : 40, nb max bulls : 10
objective tightened to 14836 lb:14835
Cost : 148
nNodes: 218
nFails: 110
time(ms): 32
completed: true
timeInTrail: 1
nSols: 1

Time : 959ms
Nb cows : 100, nb bulls : 40, nb max bulls : 10
objective tightened to 24408 lb:24407
Cost : 244
nNodes: 210
nFails: 106
time(ms): 31
completed: true
timeInTrail: 0
nSols: 1

Time : 905ms
Nb cows : 100, nb bulls : 40, nb max bulls : 10
objective tightened to 14838 lb:14837
Cost : 148
nNodes: 208
nFails: 105
time(ms): 51
completed: true
timeInTrail: 0
nSols: 1

Time : 1700ms
Nb cows : 100, nb bulls : 40, nb max bulls : 10
objective tightened to 15423 lb:15422
Cost : 154
nNodes: 210
nFails: 106
time(ms): 30
completed: true
timeInTrail: 0
nSols: 1

Time : 949ms
Nb cows : 100, nb bulls : 40, nb max bulls : 10
objective tightened to 7807 lb:7806
Cost : 78
nNodes: 208
nFails: 105
time(ms): 34
completed: true
timeInTrail: 0
nSols: 1

Time : 885ms
********************************************
