name := "oscar-cp-template-dev-snapshot"

version := "0.1"

scalaVersion := "2.12.4"

resolvers += Resolver.url("typesafe", url("http://repo.typesafe.com/typesafe/releases/"))

resolvers += "Oscar Snapshots" at "http://artifactory.info.ucl.ac.be/artifactory/libs-snapshot-local/"

libraryDependencies += "oscar" %% "oscar-cp" % "4.0.0-SNAPSHOT" withSources()
libraryDependencies += "org.apache.commons" % "commons-lang3" % "3.7"
libraryDependencies += "net.liftweb" %% "lift-json" % "3.1.0"
